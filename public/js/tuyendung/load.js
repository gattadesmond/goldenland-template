function iOSversion() {
  if (/iP(hone|od|ad)/.test(navigator.platform)) {
    var e = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
    return [parseInt(e[1], 10), parseInt(e[2], 10), parseInt(e[3] || 0, 10)];
  }
}

function ResizeWindows() {
  var e,
    t,
    i = ($(window).height() > $(window).width(),
    $(window).height() <= $(window).width(),
    $(".bg-home img")),
    n = $(window).width(),
    o = $(window).height(),
    r = o / n,
    s = 787 / 1440,
    a = 0.5625;
  if (
    (r > s ? ((t = o), (e = o / s)) : ((t = n * s), (e = n)),
    $(".container").css({ width: n }),
    $(".go-top").css({ display: "none", opacity: 0 }),
    $(".album-pic-center").css({ height: o }),
    $(".content-page, .colum-project").css({ "min-height": o / 2 }),
    1100 >= n)
  ) {
    if (
      ($(".scroll-down").css({ top: o - 70 }),
      $(".video-wrap").css({ width: "100%", height: "100%", margin: 0 }),
      420 >= n
        ? ($(i).css({
            width: n + 300,
            height: (n + 300) * s,
            left: -150,
            top: 0,
            bottom: "auto"
          }),
          $(".bg-home, .slide-bg, .slider-home").css({
            width: "100%",
            height: (n + 300) * s
          }))
        : n > 420 && 620 >= n
          ? ($(i).css({
              width: n + 200,
              height: (n + 200) * s,
              left: -100,
              top: 0,
              bottom: "auto"
            }),
            $(".bg-home, .slide-bg, .slider-home").css({
              width: "100%",
              height: (n + 200) * s
            }))
          : ($(i).css({
              width: n,
              height: n * s,
              left: 0,
              top: 0,
              bottom: "auto"
            }),
            $(".bg-home, .slide-bg, .slider-home").css({
              width: "100%",
              height: n * s
            })),
      $(".bg-cover, .bg-picture, .bg-parallax").css({
        width: n,
        height: n * s
      }),
      $('.content-center[data-hash="value"] .bg-cover').css({
        width: n,
        height: $(".value").innerHeight() + 100
      }),
      $('.content-center[data-hash="chart"] .bg-cover').css({
        width: n,
        height: $(".img-chart").innerHeight() + 100
      }),
      $(".scrollB, .scrollA, .scrollC, .scrollD, .scrollE,.scrollF,.scrollG ")
        .getNiceScroll()
        .remove(),
      $(".scrollA, .scrollC, .scrollD").css({ height: "auto" }),
      $(".content-page, .colum-box").css({ width: n, height: "auto" }),
      $(".box-content").css({ height: "auto" }),
      $("#news-page").length)
    ) {
      $(".colum-box-news").css({ width: n, "min-height": o });
      var c = $(".colum-box.active").innerHeight();
      $(".box-content").css({ height: c }),
        $(".sub-news").css({ top: $(".bg-cover, .bg-picture").height() - 70 });
    }
    $("#resource-page").length &&
      ($(".colum-box-news").css({ width: "100%", "min-height": o }),
      $(".news-list.career").css({ right: "auto" })),
      $("#project-details-page").length &&
        ($(".colum-project").css({ width: "100%" }),
        $(".box-content-project").css({ width: "100%" }),
        $(".scrollH").css({ height: "auto" }),
        $(".colum-box-news-project").css({ width: "100%", "margin-left": 0 }),
        $(".news-list-project").css({ right: "auto" }),
        $(".scrollC").css({ height: "auto" }),
        $(".house-pic").css({ height: "auto", "margin-top": 0 })),
      $("#sustainable-page").length &&
        $(".scrollC").css({ width: "100%", height: "auto" }),
      740 >= n
        ? $(
            ".news-text img, .item-box img, .album-pic-center img, .content-text img, .content p img"
          ).addClass("zoom-pic")
        : $(
            ".news-text img,  .item-box img, .album-pic-center img, .content-text img, .content p img"
          ).removeClass("zoom-pic");
  } else if (n > 1100) {
    $(".scroll-down").css({ display: "none", opacity: 0 });
    var h = o - 100,
      l = h / a;
    if (
      ($(".video-wrap").css({
        width: l,
        height: h,
        "margin-left": -l / 2,
        "margin-top": -h / 2 + 30
      }),
      $(i).css({
        width: e,
        height: t,
        left: (n - e) / 2,
        top: "auto",
        bottom: 0
      }),
      $(
        '.bg-home, .bg-cover, .content-center[data-hash="value"] .bg-cover, .content-center[data-hash="chart"] .bg-cover'
      ).css({ width: n, height: o }),
      $(".slide-bg, .slider-home").css({ width: n, height: o }),
      $(".bg-parallax").css({ width: n + n / 2, height: o }),
      $(".bg-picture").css({ width: n + 20, height: o + 20 }),
      $(
        ".news-text img,  .item-box img, .album-pic-center img, .content-text img, .content p img"
      ).removeClass("zoom-pic"),
      $(".sub-nav-block ul, .sub-nav-typical ul").css({ width: "100%" }),
      $(".content-page, .colum-box").css({ width: n, height: o }),
      $(".box-content").css({ height: o }),
      $(".colum-box-news").css({ width: n - 550, "min-height": "inherit" }),
      $(".sub-nav").css({ top: $(".title-page").innerHeight() + 300 }),
      $("#about-page").length &&
        ($(".content").each(function(e, t) {
          var i = $(t)
            .find(".scrollA")
            .innerHeight();
          i >= o - 150
            ? $(t)
                .find(".scrollA")
                .css({ height: o - 150 })
            : $(t)
                .find(".scrollA")
                .css({ height: "auto" });
        }),
        $(".content-box").each(function(e, t) {
          var i = $(t)
            .find(".scrollC")
            .innerHeight();
          i >= o - 230
            ? $(t)
                .find(".scrollC")
                .css({ height: o - 230 })
            : $(t)
                .find(".scrollC")
                .css({ height: "auto" });
        })),
      $("#member-page").length &&
        $(".member").each(function(e, t) {
          var i = $(t)
            .find(".scrollC")
            .innerHeight();
          i >= o - 270
            ? $(t)
                .find(".scrollC")
                .css({ height: o - 270 })
            : $(t)
                .find(".scrollC")
                .css({ height: "auto" });
        }),
      $("#business-page, #search-page").length &&
        $(".member").each(function(e, t) {
          var i = $(t)
            .find(".scrollC")
            .innerHeight();
          i >= o - 170
            ? $(t)
                .find(".scrollC")
                .css({ height: o - 170 })
            : $(t)
                .find(".scrollC")
                .css({ height: "auto" });
        }),
      $("#sustainable-page").length &&
        ($(".content-sustainable").each(function(e, t) {
          var i = $(t)
            .find(".scrollC")
            .innerHeight();
          i >= o - 170
            ? $(t)
                .find(".scrollC")
                .css({ height: o - 170 })
            : $(t)
                .find(".scrollC")
                .css({ height: "auto" });
        }),
        $(".scrollC").css({ width: "100%" })),
      $("#resource-page").length &&
        ($(".content-resource").each(function(e, t) {
          var i = $(t)
            .find(".scrollC")
            .innerHeight();
          i >= o - 170
            ? $(t)
                .find(".scrollC")
                .css({ height: o - 170 })
            : $(t)
                .find(".scrollC")
                .css({ height: "auto" });
        }),
        $(".scrollD").css({ height: o - 220 }),
        $(".link-page").length > 5
          ? $(".news-list.career").css({
              right: $(".colum-box-news").width() + 100
            })
          : $(".news-list.career").css({
              right: $(".colum-box-news").width() + 100
            })),
      $("#project-details-page").length)
    ) {
      $(".colum-project").css({ width: n });
      var d = $(".colum-project").length,
        u = $(".colum-project").width();
      $(".box-content-project").width(d * u),
        $(".colum-box-news-project").css({ width: n - 450 }),
        $(".colum-box-news-project").css({
          "margin-left": -$(".colum-box-news-project").width() / 2 + 20
        }),
        $(".news-list-project").css({
          right:
            $(".colum-box-news-project").width() +
            (n / 2 - $(".colum-box-news-project").width() / 2 - 8)
        });
    }
    $("#news-page").length &&
      ($(".scrollD").css({ height: o - 120 }),
      $(".sub-news").css({ top: $(".title-page").innerHeight() + 130 }));
  }
  var d = $(".colum-box").length,
    u = $(".colum-box").width();
  $(".box-content").width(d * u);
}
function initialize() {
  function e() {
    m.setAnimation(google.maps.Animation.BOUNCE);
  }
  function t() {
    m.setAnimation(null);
  }
  function i() {
    t();
    var e = document.createElement("div");
    e.innerHTML =
      "<div class='infobox'><img src='" +
      a +
      "'  alt='" +
      c +
      "' ><h3>" +
      c +
      "</h3><p>" +
      o +
      "<br>" +
      u +
      h +
      "<br>" +
      d +
      l +
      "<br></p></div>";
    var i = {
        content: e,
        disableAutoPan: !0,
        maxWidth: 250,
        pixelOffset: new google.maps.Size(-125, -150),
        boxStyle: { background: "transparent", opacity: 1, width: "300px" },
        closeBoxMargin: "0",
        closeBoxzIndex: "99999",
        closeBoxPosition: "absolute",
        closeBoxURL: n + "default/images/close3.png",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: !1,
        pane: "floatPane",
        enableEventPropagation: !0
      },
      r = new InfoBox(i);
    r.open(w, m);
  }
  var n = ($(".httpserver").text(), $(".httptemplate").text()),
    o = $(".infobox-address").text(),
    r = $(".infobox-location-lat").text(),
    s = $(".infobox-location-lng").text(),
    a = $(".infobox-image").text(),
    c = ($(".infobox-googlemap").text(), $(".infobox-name").text()),
    h = $(".infobox-phone").text(),
    l = ($(".infobox-fax").text(),
    $(".infobox-phone-tel").text(),
    $(".infobox-email").text()),
    d = $(".infobox-text-email").text(),
    u = $(".infobox-text-tel").text(),
    g = new google.maps.LatLng(r, s),
    m = null,
    p = [
      { stylers: [{ hue: "#737373" }, { gamma: 1.51 }] },
      {
        featureType: "road",
        elementType: "geometry",
        stylers: [{ lightness: -20 }, { visibility: "simplified" }]
      },
      {
        featureType: "road",
        elementType: "labels",
        stylers: [{ visibility: "off" }]
      }
    ],
    f = new google.maps.StyledMapType(p, { name: "Styled Map" }),
    v = {
      center: g,
      zoom: 15,
      scrollwheel: !0,
      draggable: !0,
      draggingCursor: "move",
      noclear: !0,
      disableDefaultUI: !0,
      disableDoubleClickZoom: !0,
      mapTypeControlOptions: {
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, "map_style"],
        position: google.maps.ControlPosition.TOP_RIGHT
      }
    };
  google.maps.event.addDomListener(window, "resize", function() {
    google.maps.event.trigger(w, "resize"), w.setCenter(g), w.setZoom(15);
  });
  var w = new google.maps.Map(document.getElementById("map-canvas"), v);
  w.mapTypes.set("map_style", f), w.setMapTypeId("map_style");
  var b = n + "default/images/logo-map.png";
  (m = new google.maps.Marker({
    map: w,
    draggable: !1,
    animation: google.maps.Animation.DROP,
    position: new google.maps.LatLng(r, s),
    icon: b
  })),
    $(window).width() > 1100 &&
      (google.maps.event.addListener(m, "mouseover", e),
      google.maps.event.addListener(m, "mouseout", t)),
    google.maps.event.addListener(m, "click", i),
    ZoomControl(w);
}
function ZoomControl(e) {
  $(".zoom-control a").click(function() {
    var t = e.getZoom();
    switch ($(this).attr("id")) {
      case "zoom-in":
        e.setZoom(++t);
        break;
      case "zoom-out":
        e.setZoom(--t);
    }
    return !1;
  });
}
function LoadProjects() {
  if ($(window).width() > 1100) {
    var e = $(".grid");
    e.imagesLoaded(function() {
      $(".pic-project").addClass("arrange"),
        e.isotope({
          itemSelector: ".pic-project",
          stagger: 50,
          percentPosition: !0
        }),
        e.isotope("shuffle"),
        $(".loadicon").fadeOut(500, function() {
          $(".loadicon").remove();
        });
    });
  } else
    $(".grid").addClass("slide-pro"),
      SlidePicture(),
      $(".loadicon").fadeOut(500, function() {
        $(".loadicon").remove();
      });
}
function Calculation() {
  var e = $(window).width();
  $(window).height();
  if (1100 >= e) {
    var t = 0;
    $(".sub-news li, .sub-menu li, .sub-nav-typical li").each(function() {
      var e = $(this).outerWidth() + 4;
      (t += parseInt(e)),
        $(".sub-news ul, .sub-menu ul, .sub-nav-typical ul").width(t);
    }),
      $(".number-slider").each(function(e, t) {
        var i = 50,
          n = $(t).children().length;
        $(this).width(i * n);
      }),
      $(".scrollB").each(function() {
        var e = $(this).children().length,
          t =
            $(this)
              .children()
              .width() + 5;
        $(this).width(e * t);
      });
    var i = $(".popup-pics-mobile img").attr("src");
    $(".popup-img").css({ "background-image": "url(  " + i + "  )" });
  } else
    $(".sub-news ul, .sub-menu ul, .sub-nav-typical ul, .number-slider").css({
      width: "100%"
    }),
      $(".scrollB").css({ width: "100%" });
}
function Done() {
  $("html, body").scrollTop(0),
    ResizeWindows(),
    Calculation(),
    $(".title-page > h1")
      .lettering("words")
      .children("span")
      .lettering()
      .children("span")
      .lettering(),
    $(".left-content h3")
      .lettering("words")
      .children("span")
      .lettering(),
    ContentLoad(),
    $(window).width() > 1100
      ? $(".container").addClass("fadein")
      : $(".container").addClass("show"),
    $(".grid").length
      ? LoadProjects()
      : $(".loadicon").fadeOut(500, function() {
          $(".loadicon").remove();
        });
}
var ua = navigator.userAgent,
  match = ua.match("MSIE (.)"),
  versions = match && match.length > 1 ? match[1] : "unknown",
  isTouchDevice =
    "ontouchstart" in window ||
    (window.DocumentTouch && document instanceof DocumentTouch) ||
    navigator.msMaxTouchPoints > 0 ||
    navigator.maxTouchPoints > 0,
  isDesktop = 0 != $(window).width() && !isTouchDevice,
  IEMobile = ua.match(/IEMobile/i),
  isIE9 = /MSIE 9/i.test(ua),
  isIE10 = /MSIE 10/i.test(ua),
  isIE11 = !(!/rv:11.0/i.test(ua) || IEMobile),
  isOpera =
    (!!window.opr && !!opr.addons) ||
    !!window.opera ||
    ua.indexOf(" OPR/") >= 0,
  isFirefox = "undefined" != typeof InstallTrigger,
  isIE = !!document.documentMode,
  isEdge = !isIE && !!window.StyleMedia && !isIE11,
  isChrome = !!window.chrome && !!window.chrome.webstore,
  isBlink = (isChrome || isOpera) && !!window.CSS,
  isSafari =
    /constructor/i.test(window.HTMLElement) && !ua.match(" Version/5."),
  isSafari5 =
    ua.match("Safari/") && !ua.match("Chrome") && ua.match(" Version/5."),
  AndroidVersion = parseFloat(ua.slice(ua.indexOf("Android") + 8)),
  Version = ua.match(/Android\s([0-9\.]*)/i),
  isIOS8 = function() {
    var e = navigator.userAgent.toLowerCase();
    return /iphone os 8_/.test(e);
  },
  iOS = iOSversion(),
  ios,
  android,
  blackBerry,
  UCBrowser,
  Operamini,
  firefox,
  windows,
  smartphone,
  tablet,
  touchscreen,
  all,
  isMobile = {
    ios: (function() {
      return ua.match(/iPhone|iPad|iPod/i);
    })(),
    android: (function() {
      return ua.match(/Android/i);
    })(),
    blackBerry: (function() {
      return ua.match(/BB10|Tablet|Mobile/i);
    })(),
    UCBrowser: (function() {
      return ua.match(/UCBrowser/i);
    })(),
    Operamini: (function() {
      return ua.match(/Opera Mini/i);
    })(),
    windows: (function() {
      return ua.match(/IEMobile/i);
    })(),
    smartphone: (function() {
      return (
        ua.match(
          /Android|BlackBerry|Tablet|Mobile|iPhone|iPad|iPod|Opera Mini|IEMobile/i
        ) &&
        window.innerWidth <= 440 &&
        window.innerHeight <= 740
      );
    })(),
    tablet: (function() {
      return (
        ua.match(
          /Android|BlackBerry|Tablet|Mobile|iPhone|iPad|iPod|Opera Mini|IEMobile/i
        ) &&
        window.innerWidth <= 1366 &&
        window.innerHeight <= 800
      );
    })(),
    all: (function() {
      return ua.match(
        /Android|BlackBerry|Tablet|Mobile|iPhone|iPad|iPod|Opera Mini|IEMobile/i
      );
    })()
  };
if (isTouchDevice && null !== isMobile.all) var TouchLenght = !0;
else if ((isMobile.tablet && isFirefox) || (isMobile.smartphone && isFirefox))
  var TouchLenght = !0;
else var TouchLenght = !1;
isMobile.Operamini && alert("Please disable Data Savings Mode");
var AnimEnd = "webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend",
  Loadx = 0;
!(function(e) {
  Array.prototype.indexOf ||
    (Array.prototype.indexOf = function(e) {
      var t = this.length >>> 0,
        i = Number(arguments[1]) || 0;
      for (
        i = 0 > i ? Math.ceil(i) : Math.floor(i), 0 > i && (i += t);
        t > i;
        i++
      )
        if (i in this && this[i] === e) return i;
      return -1;
    });
  var t = (e(window).height(), e(window).width(), new Array()),
    i = 0,
    n = !1,
    o = "",
    r = "",
    s = "",
    a = "",
    c = 0,
    h = 0,
    l = {
      onComplete: function() {
        e("#qLoverlay").remove(), e("body .item-load").remove();
      },
      backgroundColor: "#fff",
      barColor: "#fff",
      barHeight: 1,
      percentage: !0,
      deepSearch: !0,
      completeAnimation: "fade",
      minimumTime: 500,
      onLoadComplete: function() {
        if ("grow" == l.completeAnimation) {
          var t = 100,
            i = new Date();
          i.getTime() - h < l.minimumTime &&
            (t = l.minimumTime - (i.getTime() - h)),
            e("#qLbar")
              .stop()
              .animate({ width: "100%" }, t, function() {
                e("#qLoverlay").fadeOut(200, function() {
                  l.onComplete(),
                    0 == Loadx && ((Loadx = 1), Done()),
                    ResizeWindows();
                });
              });
        }
      }
    },
    d = function() {
      var e = new Date();
      (h = e.getTime()), u(), f();
    },
    u = function() {
      o = e('<div class="item-load"></div>')
        .appendTo("body")
        .css({ display: "none", width: 0, height: 0, overflow: "hidden" });
      for (var i = 0; t.length > i; i++)
        e.ajax({
          url: t[i],
          type: "HEAD",
          success: function() {
            n || (c++, g(this.url));
          }
        });
    },
    g = function(t) {
      e("<img/>")
        .attr("src", t)
        .bind("load", function() {
          m();
        })
        .appendTo(o);
    },
    m = function() {
      i++;
      var t = (i / c) * 100;
      e(s)
        .stop()
        .animate({ width: t + "%", minWidth: t + "%" }, 200),
        1 == l.percentage && e(a).text(Math.ceil(t) + "%"),
        i == c && p();
    },
    p = function() {
      e(o).remove(), l.onLoadComplete(), (n = !0);
    },
    f = function() {
      (r = e('<div id="qLoverlay"></div>')
        .css({
          width: "100%",
          height: "10px",
          position: "absolute",
          zIndex: 1e3,
          top: 0,
          left: 0
        })
        .appendTo("body")),
        (s = e('<div id="qLbar"></div>')
          .css({
            height: l.barHeight + "px",
            backgroundColor: l.barColor,
            width: "0%",
            position: "absolute",
            top: "0px"
          })
          .appendTo(r)),
        1 == l.percentage &&
          (a = e('<div id="qLpercentage"></div>')
            .text("0%")
            .css({
              height: "120px",
              width: "120px",
              position: "absolute",
              fontSize: "0px",
              top: "50%",
              left: "50%",
              marginTop: "60px",
              textAlign: "center",
              marginLeft: "-60px",
              color: "#fff"
            })
            .appendTo(r));
    },
    v = function(i) {
      var n = "";
      if ("none" != e(i).css("background-image"))
        var n = e(i).css("background-image");
      else if (
        "undefined" != typeof e(i).attr("src") &&
        "img" == i.nodeName.toLowerCase()
      )
        var n = e(i).attr("src");
      if (-1 == n.indexOf("gradient")) {
        (n = n.replace(/url\(\"/g, "")),
          (n = n.replace(/url\(/g, "")),
          (n = n.replace(/\"\)/g, "")),
          (n = n.replace(/\)/g, ""));
        for (var o = n.split(", "), r = 0; r < o.length; r++)
          if (o[r].length > 0 && -1 == t.indexOf(o[r])) {
            var s = "";
            t.push(o[r] + s);
          }
      }
    };
  e.fn.queryLoader = function(t) {
    return (
      t && e.extend(l, t),
      this.each(function() {
        v(this),
          1 == l.deepSearch &&
            e(this)
              .find("*:not(script)")
              .each(function() {
                v(this);
              });
      }),
      d(),
      this
    );
  };
})(jQuery),
  $(document).ready(function() {
    $("html, body").scrollTop(0),
      ResizeWindows(),
      (isChrome || isSafari || isMobile.ios || isMobile.android) &&
        $("head").append(

        ),
      $(".loadicon").length ||
        $("body").append('<div class="loadicon" style="display:block"></div>'),
      $("body").queryLoader({
        barColor: "#fff",
        percentage: !1,
        barHeight: 1,
        completeAnimation: "grow",
        minimumTime: 100
      }),
      setTimeout(function() {
        0 == Loadx && ((Loadx = 1), Done());
      }, 500);
  }),
  function() {
    "use strict";
    function e() {}
    function t(e, t) {
      for (var i = e.length; i--; ) if (e[i].listener === t) return i;
      return -1;
    }
    function i(e) {
      return function() {
        return this[e].apply(this, arguments);
      };
    }
    var n = e.prototype,
      o = this,
      r = o.EventEmitter;
    (n.getListeners = function(e) {
      var t,
        i,
        n = this._getEvents();
      if ("object" == typeof e) {
        t = {};
        for (i in n) n.hasOwnProperty(i) && e.test(i) && (t[i] = n[i]);
      } else t = n[e] || (n[e] = []);
      return t;
    }),
      (n.flattenListeners = function(e) {
        var t,
          i = [];
        for (t = 0; t < e.length; t += 1) i.push(e[t].listener);
        return i;
      }),
      (n.getListenersAsObject = function(e) {
        var t,
          i = this.getListeners(e);
        return i instanceof Array && ((t = {}), (t[e] = i)), t || i;
      }),
      (n.addListener = function(e, i) {
        var n,
          o = this.getListenersAsObject(e),
          r = "object" == typeof i;
        for (n in o)
          o.hasOwnProperty(n) &&
            -1 === t(o[n], i) &&
            o[n].push(r ? i : { listener: i, once: !1 });
        return this;
      }),
      (n.on = i("addListener")),
      (n.addOnceListener = function(e, t) {
        return this.addListener(e, { listener: t, once: !0 });
      }),
      (n.once = i("addOnceListener")),
      (n.defineEvent = function(e) {
        return this.getListeners(e), this;
      }),
      (n.defineEvents = function(e) {
        for (var t = 0; t < e.length; t += 1) this.defineEvent(e[t]);
        return this;
      }),
      (n.removeListener = function(e, i) {
        var n,
          o,
          r = this.getListenersAsObject(e);
        for (o in r)
          r.hasOwnProperty(o) &&
            ((n = t(r[o], i)), -1 !== n && r[o].splice(n, 1));
        return this;
      }),
      (n.off = i("removeListener")),
      (n.addListeners = function(e, t) {
        return this.manipulateListeners(!1, e, t);
      }),
      (n.removeListeners = function(e, t) {
        return this.manipulateListeners(!0, e, t);
      }),
      (n.manipulateListeners = function(e, t, i) {
        var n,
          o,
          r = e ? this.removeListener : this.addListener,
          s = e ? this.removeListeners : this.addListeners;
        if ("object" != typeof t || t instanceof RegExp)
          for (n = i.length; n--; ) r.call(this, t, i[n]);
        else
          for (n in t)
            t.hasOwnProperty(n) &&
              (o = t[n]) &&
              ("function" == typeof o
                ? r.call(this, n, o)
                : s.call(this, n, o));
        return this;
      }),
      (n.removeEvent = function(e) {
        var t,
          i = typeof e,
          n = this._getEvents();
        if ("string" === i) delete n[e];
        else if ("object" === i)
          for (t in n) n.hasOwnProperty(t) && e.test(t) && delete n[t];
        else delete this._events;
        return this;
      }),
      (n.removeAllListeners = i("removeEvent")),
      (n.emitEvent = function(e, t) {
        var i,
          n,
          o,
          r,
          s = this.getListenersAsObject(e);
        for (o in s)
          if (s.hasOwnProperty(o))
            for (n = s[o].length; n--; )
              (i = s[o][n]),
                i.once === !0 && this.removeListener(e, i.listener),
                (r = i.listener.apply(this, t || [])),
                r === this._getOnceReturnValue() &&
                  this.removeListener(e, i.listener);
        return this;
      }),
      (n.trigger = i("emitEvent")),
      (n.emit = function(e) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(e, t);
      }),
      (n.setOnceReturnValue = function(e) {
        return (this._onceReturnValue = e), this;
      }),
      (n._getOnceReturnValue = function() {
        return this.hasOwnProperty("_onceReturnValue")
          ? this._onceReturnValue
          : !0;
      }),
      (n._getEvents = function() {
        return this._events || (this._events = {});
      }),
      (e.noConflict = function() {
        return (o.EventEmitter = r), e;
      }),
      "function" == typeof define && define.amd
        ? define("eventEmitter/EventEmitter", [], function() {
            return e;
          })
        : "object" == typeof module && module.exports
          ? (module.exports = e)
          : (this.EventEmitter = e);
  }.call(this),
  (function(e) {
    function t(t) {
      var i = e.event;
      return (i.target = i.target || i.srcElement || t), i;
    }
    var i = document.documentElement,
      n = function() {};
    i.addEventListener
      ? (n = function(e, t, i) {
          e.addEventListener(t, i, !1);
        })
      : i.attachEvent &&
        (n = function(e, i, n) {
          (e[i + n] = n.handleEvent
            ? function() {
                var i = t(e);
                n.handleEvent.call(n, i);
              }
            : function() {
                var i = t(e);
                n.call(e, i);
              }),
            e.attachEvent("on" + i, e[i + n]);
        });
    var o = function() {};
    i.removeEventListener
      ? (o = function(e, t, i) {
          e.removeEventListener(t, i, !1);
        })
      : i.detachEvent &&
        (o = function(e, t, i) {
          e.detachEvent("on" + t, e[t + i]);
          try {
            delete e[t + i];
          } catch (n) {
            e[t + i] = void 0;
          }
        });
    var r = { bind: n, unbind: o };
    "function" == typeof define && define.amd
      ? define("eventie/eventie", r)
      : (e.eventie = r);
  })(this),
  (function(e, t) {
    "use strict";
    "function" == typeof define && define.amd
      ? define(["eventEmitter/EventEmitter", "eventie/eventie"], function(
          i,
          n
        ) {
          return t(e, i, n);
        })
      : "object" == typeof module && module.exports
        ? (module.exports = t(
            e,
            require("wolfy87-eventemitter"),
            require("eventie")
          ))
        : (e.imagesLoaded = t(e, e.EventEmitter, e.eventie));
  })(window, function(e, t, i) {
    function n(e, t) {
      for (var i in t) e[i] = t[i];
      return e;
    }
    function o(e) {
      return "[object Array]" == d.call(e);
    }
    function r(e) {
      var t = [];
      if (o(e)) t = e;
      else if ("number" == typeof e.length)
        for (var i = 0; i < e.length; i++) t.push(e[i]);
      else t.push(e);
      return t;
    }
    function s(e, t, i) {
      if (!(this instanceof s)) return new s(e, t, i);
      "string" == typeof e && (e = document.querySelectorAll(e)),
        (this.elements = r(e)),
        (this.options = n({}, this.options)),
        "function" == typeof t ? (i = t) : n(this.options, t),
        i && this.on("always", i),
        this.getImages(),
        h && (this.jqDeferred = new h.Deferred());
      var o = this;
      setTimeout(function() {
        o.check();
      });
    }
    function a(e) {
      this.img = e;
    }
    function c(e, t) {
      (this.url = e), (this.element = t), (this.img = new Image());
    }
    var h = e.jQuery,
      l = e.console,
      d = Object.prototype.toString;
    (s.prototype = new t()),
      (s.prototype.options = {}),
      (s.prototype.getImages = function() {
        this.images = [];
        for (var e = 0; e < this.elements.length; e++) {
          var t = this.elements[e];
          this.addElementImages(t);
        }
      }),
      (s.prototype.addElementImages = function(e) {
        "IMG" == e.nodeName && this.addImage(e),
          this.options.background === !0 && this.addElementBackgroundImages(e);
        var t = e.nodeType;
        if (t && u[t]) {
          for (var i = e.querySelectorAll("img"), n = 0; n < i.length; n++) {
            var o = i[n];
            this.addImage(o);
          }
          if ("string" == typeof this.options.background) {
            var r = e.querySelectorAll(this.options.background);
            for (n = 0; n < r.length; n++) {
              var s = r[n];
              this.addElementBackgroundImages(s);
            }
          }
        }
      });
    var u = { 1: !0, 9: !0, 11: !0 };
    s.prototype.addElementBackgroundImages = function(e) {
      for (
        var t = g(e),
          i = /url\(['"]*([^'"\)]+)['"]*\)/gi,
          n = i.exec(t.backgroundImage);
        null !== n;

      ) {
        var o = n && n[1];
        o && this.addBackground(o, e), (n = i.exec(t.backgroundImage));
      }
    };
    var g =
      e.getComputedStyle ||
      function(e) {
        return e.currentStyle;
      };
    return (
      (s.prototype.addImage = function(e) {
        var t = new a(e);
        this.images.push(t);
      }),
      (s.prototype.addBackground = function(e, t) {
        var i = new c(e, t);
        this.images.push(i);
      }),
      (s.prototype.check = function() {
        function e(e, i, n) {
          setTimeout(function() {
            t.progress(e, i, n);
          });
        }
        var t = this;
        if (
          ((this.progressedCount = 0),
          (this.hasAnyBroken = !1),
          !this.images.length)
        )
          return void this.complete();
        for (var i = 0; i < this.images.length; i++) {
          var n = this.images[i];
          n.once("progress", e), n.check();
        }
      }),
      (s.prototype.progress = function(e, t, i) {
        this.progressedCount++,
          (this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded),
          this.emit("progress", this, e, t),
          this.jqDeferred &&
            this.jqDeferred.notify &&
            this.jqDeferred.notify(this, e),
          this.progressedCount == this.images.length && this.complete(),
          this.options.debug && l && l.log("progress: " + i, e, t);
      }),
      (s.prototype.complete = function() {
        var e = this.hasAnyBroken ? "fail" : "done";
        if (
          ((this.isComplete = !0),
          this.emit(e, this),
          this.emit("always", this),
          this.jqDeferred)
        ) {
          var t = this.hasAnyBroken ? "reject" : "resolve";
          this.jqDeferred[t](this);
        }
      }),
      (a.prototype = new t()),
      (a.prototype.check = function() {
        var e = this.getIsImageComplete();
        return e
          ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth")
          : ((this.proxyImage = new Image()),
            i.bind(this.proxyImage, "load", this),
            i.bind(this.proxyImage, "error", this),
            i.bind(this.img, "load", this),
            i.bind(this.img, "error", this),
            void (this.proxyImage.src = this.img.src));
      }),
      (a.prototype.getIsImageComplete = function() {
        return this.img.complete && void 0 !== this.img.naturalWidth;
      }),
      (a.prototype.confirm = function(e, t) {
        (this.isLoaded = e), this.emit("progress", this, this.img, t);
      }),
      (a.prototype.handleEvent = function(e) {
        var t = "on" + e.type;
        this[t] && this[t](e);
      }),
      (a.prototype.onload = function() {
        this.confirm(!0, "onload"), this.unbindEvents();
      }),
      (a.prototype.onerror = function() {
        this.confirm(!1, "onerror"), this.unbindEvents();
      }),
      (a.prototype.unbindEvents = function() {
        i.unbind(this.proxyImage, "load", this),
          i.unbind(this.proxyImage, "error", this),
          i.unbind(this.img, "load", this),
          i.unbind(this.img, "error", this);
      }),
      (c.prototype = new a()),
      (c.prototype.check = function() {
        i.bind(this.img, "load", this),
          i.bind(this.img, "error", this),
          (this.img.src = this.url);
        var e = this.getIsImageComplete();
        e &&
          (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"),
          this.unbindEvents());
      }),
      (c.prototype.unbindEvents = function() {
        i.unbind(this.img, "load", this), i.unbind(this.img, "error", this);
      }),
      (c.prototype.confirm = function(e, t) {
        (this.isLoaded = e), this.emit("progress", this, this.element, t);
      }),
      (s.makeJQueryPlugin = function(t) {
        (t = t || e.jQuery),
          t &&
            ((h = t),
            (h.fn.imagesLoaded = function(e, t) {
              var i = new s(this, e, t);
              return i.jqDeferred.promise(h(this));
            }));
      }),
      s.makeJQueryPlugin(),
      s
    );
  });
