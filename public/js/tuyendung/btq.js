function NavClick() {
  $(".nav-click").bind("click", function() {
    return (
      $(this).hasClass("active")
        ? ($(".navigation").scrollTop(0),
          $(".nav-click").removeClass("active"),
          $(".overlay-menu, .navigation").removeClass("show"),
          $("html, body").removeClass("no-scroll-nav"))
        : ($(".top").scrollTop(0),
          $(".nav-click").addClass("active"),
          $(".overlay-menu, .navigation").addClass("show"),
          $("html, body").addClass("no-scroll-nav")),
      !1
    );
  });
}
function execSearch() {
  var e = $("#qsearch").val(),
    t = $("#href_search").val(),
    i = $("#defaultvalue").val(),
    i = $("#defaultvalue").val(),
    o = $("#errorsearch").val();
  if ((hidemsg(), e == i || "" == e)) return !1;
  if (e.length <= 1)
    return (
      $("html, body").animate({ scrollTop: 0 }, "slow"),
      $(".overlay-dark").after(
        "<div  class='contact-success color-red'>" + o + "</div>"
      ),
      setTimeout(hidemsg, 5e3),
      !1
    );
  if ("" != e) {
    var a = t + "?qsearch=" + encodeURIComponent(e);
    return (window.location = a), !1;
  }
}
function Search() {
  $("a.search").bind("click", function() {
    $(window).width() > 1100
      ? 1 == show
        ? ($(".form-row-search").css({ width: 0 }),
          $(".search-form").css({ width: 0, opacity: 0 }),
          $("a.search").removeClass("active"),
          (show = 0),
          execSearch())
        : ($(".form-row-search").css({ width: "100%" }),
          $(".search-form").css({ width: 280, opacity: 1 }),
          $("a.search").addClass("active"),
          document.getElementById("search").reset(),
          (show = 1))
      : ($(".form-row-search").css({ width: "100%" }),
        $(".search-form").css({ width: "80%", opacity: 1 }),
        1 == show
          ? ($("a.search").removeClass("active"), (show = 0), execSearch())
          : ($("a.search").addClass("active"),
            document.getElementById("search").reset(),
            (show = 1)));
  }),
    $("#qsearch").keydown(function(e) {
      13 == e.keyCode && execSearch();
    });
}
function execSearch1() {
  var e = $("#qsearch1").val(),
    t = $("#href_search1").val(),
    i = $("#defaultvalue1").val(),
    i = $("#defaultvalue1").val(),
    o = $("#errorsearch1").val();
  if ((hidemsg(), e == i || "" == e)) return !1;
  if (e.length <= 1)
    return (
      $("html, body").animate({ scrollTop: 0 }, "slow"),
      $(".overlay-dark").after(
        "<div  class='contact-success color-red'>" + o + "</div>"
      ),
      setTimeout(hidemsg, 5e3),
      !1
    );
  if ("" != e) {
    var a = t + "?qsearch=" + encodeURIComponent(e);
    return (window.location = a), !1;
  }
}
function Search1() {
  $("a.search1").bind("click", function() {
    $(window).width() > 1100
      ? 1 == show1
        ? ($(".form-row-search").css({ width: 0 }),
          $(".search-form").css({ width: 0, opacity: 0 }),
          $("a.search1").removeClass("active"),
          (show1 = 0),
          execSearch1())
        : ($(".form-row-search").css({ width: "100%" }),
          $(".search-form").css({ width: 280, opacity: 1 }),
          $("a.search1").addClass("active"),
          document.getElementById("search1").reset(),
          (show1 = 1))
      : ($(".form-row-search").css({ width: "100%" }),
        $(".search-form").css({ width: "80%", opacity: 1 }),
        execSearch1());
  }),
    $("#qsearch1").keydown(function(e) {
      13 == e.keyCode && execSearch1();
    });
}
function SlidePicture() {
  if ($("#home-page").length) {
    if ($(".slider-home").length) {
      var e = $(".slider-home").attr("data-time"),
        t = new Swiper(".slide-bg", {
          pagination: ".pagination",
          autoplay: e,
          speed: 800,
          paginationClickable: !0,
          centeredSlides: !0,
          slidesPerView: 1,
          keyboardControl: !0,
          loop: !0,
          simulateTouch: !0,
          effect: "fade",
          onInit: function(e) {
            $(".slider-home, .bg-nav").addClass("fadein"),
              $(".bg-home")
                .eq(e.activeIndex)
                .addClass("show-text"),
              addMove(),
              $(".bg-home img").addClass("move");
          },
          onTransitionStart: function() {
            $(".bg-home").removeClass("show-text");
          },
          onTransitionEnd: function(e) {
            $(".bg-home")
              .eq(e.activeIndex)
              .addClass("show-text"),
              addMove(),
              $(".bg-home img").addClass("move");
          }
        });
      setTimeout(function() {
        t.once("onInit");
      }, 300);
    }
    if ($(window).width() > 1100) {
      var i = new Swiper(".home-slide", {
        speed: 800,
        paginationClickable: !0,
        nextButton: ".button-next",
        prevButton: ".button-prev",
        direction: "vertical",
        slidesPerView: 1,
        keyboardControl: !0,
        mousewheelControl: !0,
        simulateTouch: !0,
        onInit: function() {
          $(".sustain, .slide-news, .video-home")
            .addClass("fadeout")
            .removeClass("fadeinup");
        },
        onTransitionStart: function() {
          $(".item-container").removeClass("play-home"),
            $(".bg-home img").removeClass("move");
          var e = $(".slide-bg")[0].swiper;
          e.stopAutoplay(),
            $(".slider-home").hasClass("item-active")
              ? $(".scroll-down-desktop").addClass("show")
              : $(".scroll-down-desktop").removeClass("show"),
            $(".sustain, .slide-news, .video-home")
              .addClass("fadeout")
              .removeClass("fadeinup");
        },
        onTransitionEnd: function() {
          if (
            ($(".item-active").addClass("play-home"),
            $(".slider-home").hasClass("item-active"))
          ) {
            $(".bg-home img").addClass("move");
            var e = $(".slide-bg")[0].swiper;
            e.slideNext(), e.startAutoplay();
          }
          var t = $(
            ".item-active .sustain,.item-active .slide-news,.item-active .video-home"
          );
          $(t).each(function(e) {
            var t = $(this);
            setTimeout(function() {
              $(t)
                .removeClass("fadeout")
                .addClass("fadeinup");
            }, 100 * (e + 1));
          });
        }
      });
      i.once("onInit");
    }
    $(".slide-news").BTQSlider({
      autoPlay: 4e3,
      navigation: !1,
      pagination: !0,
      autoHeight: !0,
      itemsCustom: [[0, 1], [400, 1], [500, 2], [1e3, 2]],
      afterAction: function() {
        this.$BTQItems.removeClass("show-news"),
          this.$BTQItems.eq(this.currentItem).addClass("show-news");
      }
    }),
      $(window).width() <= 1100 &&
        ($(".wrap-sustainable").BTQSlider({
          navigation: !1,
          pagination: !0,
          itemsCustom: [[0, 1], [400, 1], [500, 2], [1e3, 2]],
          afterAction: function() {
            this.$BTQItems.removeClass("show-box"),
              this.$BTQItems.eq(this.currentItem).addClass("show-box");
          }
        }),
        $(".wrap-project").BTQSlider({
          singleItem: !0,
          navigation: !1,
          pagination: !0,
          afterInit: function(e) {
            $(e).trigger("BTQ.goTo", 1);
          },
          afterAction: function() {
            this.$BTQItems.removeClass("show-pro"),
              this.$BTQItems.eq(this.currentItem).addClass("show-pro");
          }
        }));
  }
  if ($("#about-page").length && $(window).width() > 1100) {
    if ($(".history-slide").length) {
      new Swiper(".history-slide", {
        speed: 600,
        paginationClickable: !0,
        centeredSlides: !0,
        slidesPerView: 1,
        keyboardControl: !0,
        mousewheelControl: !0,
        simulateTouch: !0,
        nextButton: ".button-next",
        prevButton: ".button-prev",
        effect: "slide",
        onInit: function(e) {
          $(".year")
            .eq(e.activeIndex)
            .addClass("select-history");
          var t = $(".select-history").attr("data-target");
          $('.history-sub li a[data-year= "' + t + '"]')
            .parent()
            .addClass("current"),
            $(".year")
              .find(".scrollC")
              .children()
              .addClass("fadeout")
              .removeClass("fadeinup"),
            $(".select-history .scrollC")
              .children()
              .each(function() {
                var e = $(this);
                $(e)
                  .addClass("fadeinup")
                  .removeClass("fadeout");
              });
          var i = $(".select-history")
            .find(".scrollC")
            .innerHeight();
          i >= $(window).height() - 230 &&
            ($(".scrollC").scrollTop(0),
            setTimeout(function() {
              ScrollNiceC();
            }, 800));
        },
        onTransitionStart: function() {
          $(".year").removeClass("select-history"),
            $(".history-sub li").removeClass("current"),
            $(".year")
              .find(".scrollC")
              .children()
              .addClass("fadeout")
              .removeClass("fadeinup"),
            ScrollNiceHide();
        },
        onTransitionEnd: function(e) {
          $(".year")
            .eq(e.activeIndex)
            .addClass("select-history");
          var t = $(".select-history").attr("data-target");
          $('.history-sub li a[data-year= "' + t + '"]')
            .parent()
            .addClass("current"),
            $(".select-history .scrollC")
              .children()
              .each(function() {
                var e = $(this);
                $(e)
                  .addClass("fadeinup")
                  .removeClass("fadeout");
              });
          var i = $(".select-history")
            .find(".scrollC")
            .innerHeight();
          i >= $(window).height() - 230 &&
            ($(".scrollC").scrollTop(0),
            setTimeout(function() {
              ScrollNiceC();
            }, 800));
        }
      });
    }
    var o = new Swiper(".slider-about", {
      speed: 800,
      paginationClickable: !0,
      direction: "vertical",
      hashnav: !0,
      slidesPerView: 1,
      keyboardControl: !0,
      mousewheelControl: !0,
      simulateTouch: !0,
      effect: "slide",
      onInit: function(e) {
        $(".slider-about").addClass("fadein"),
          $(".content-center").removeClass("ani-text"),
          $(".content-center")
            .eq(e.activeIndex)
            .addClass("ani-text");
        var t = $(".ani-text").attr("data-hash");
        $('.sub-nav li a[data-name= "' + t + '"]')
          .parent()
          .addClass("current"),
          setTimeout(function() {
            ScrollNiceA();
          }, 300);
        var i = $(".item-active .scrollC").innerHeight();
        i >= $(window).height() - 230 &&
          ($(".scrollC").scrollTop(0),
          setTimeout(function() {
            ScrollNiceC();
          }, 800));
      },
      onTransitionStart: function() {
        $(".content-center").removeClass("ani-text"),
          $(".year").removeClass("select-history"),
          $(".year")
            .find(".scrollC")
            .children()
            .addClass("fadeout")
            .removeClass("fadeinup"),
          $(".sub-nav li").removeClass("current"),
          $(".img-chart .zoom").removeClass("show"),
          ScrollNiceHide();
      },
      onTransitionEnd: function(e) {
        $(".content-center")
          .eq(e.activeIndex)
          .addClass("ani-text"),
          setTimeout(function() {
            ScrollNiceA();
          }, 300);
        var t = $(".development .scrollC").innerHeight();
        t >= $(window).height() - 230 &&
          ($(".scrollC").scrollTop(0),
          setTimeout(function() {
            ScrollNiceC();
          }, 800)),
          setTimeout(function() {
            $(".ani-text .img-chart .zoom").addClass("show");
          }, 1500);
        var i = $(".ani-text").attr("data-hash");
        if (
          ($('.sub-nav li a[data-name= "' + i + '"]')
            .parent()
            .addClass("current"),
          $(".sub-nav li:last-child").hasClass("current")
            ? $(".scroll-down-desktop").removeClass("show")
            : $(".scroll-down-desktop").addClass("show"),
          "history" == i)
        ) {
          $(".year.item-active").addClass("select-history"),
            $(".select-history .scrollC")
              .children()
              .each(function() {
                var e = $(this);
                $(e)
                  .addClass("fadeinup")
                  .removeClass("fadeout");
              });
          var t = $(".select-history .scrollC").innerHeight();
          t >= $(window).height() - 230 &&
            setTimeout(function() {
              ScrollNiceC();
            }, 800);
        }
      }
    });
    setTimeout(function() {
      o.once("onInit");
    }, 100),
      $(".history-slide").on("mousewheel", function(e) {
        $(window).width() > 1100 &&
          ($(".history-sub li:last-child").hasClass("current") &&
            $(".sub-nav li.current")
              .next()
              .trigger("click"),
          $(".history-sub li:first-child").hasClass("current") &&
            $(".sub-nav li.current")
              .prev()
              .trigger("click")),
          e.preventDefault();
      });
  }
  if ($("#sustainable-page").length)
    if ($(window).width() > 1100) {
      var a = new Swiper(".slider-sustainable", {
        speed: 800,
        paginationClickable: !0,
        direction: "vertical",
        hashnav: !0,
        slidesPerView: 1,
        keyboardControl: !0,
        mousewheelControl: !0,
        simulateTouch: !0,
        effect: "slide",
        onInit: function(e) {
          $(".slider-sustainable").addClass("fadein"),
            $(".content-center").removeClass("ani-sus"),
            $(".content-center")
              .eq(e.activeIndex)
              .addClass("ani-sus");
          var t = $(".ani-sus")
            .find(".scrollC")
            .innerHeight();
          t >= $(window).height() - 170 &&
            ($(".scrollC").scrollTop(0),
            setTimeout(function() {
              ScrollNiceC();
            }, 800));
        },
        onTransitionStart: function() {
          $(".content-sustainable")
            .removeClass("flipoutx_1")
            .removeClass("fadeinup_1"),
            $(".content-center").removeClass("ani-sus"),
            $(".sub-nav li").removeClass("current"),
            ScrollNiceHide();
        },
        onTransitionEnd: function(e) {
          $(".content-center")
            .eq(e.activeIndex)
            .addClass("ani-sus");
          var t = $(".ani-sus")
            .find(".scrollC")
            .innerHeight();
          t >= $(window).height() - 170 &&
            ($(".scrollC").scrollTop(0),
            setTimeout(function() {
              ScrollNiceC();
            }, 800));
          var i = $(".ani-sus").attr("data-hash");
          $('.sub-nav li a[data-name= "' + i + '"]')
            .parent()
            .addClass("current"),
            $(".sub-nav li:last-child").hasClass("current")
              ? $(".scroll-down-desktop").removeClass("show")
              : $(".scroll-down-desktop").addClass("show");
        }
      });
      setTimeout(function() {
        a.once("onInit");
      }, 100),
        $(".number-slider").each(function(e, t) {
          $(t).find(".number-item").length <= 1 &&
            $(t)
              .parent()
              .css({ display: "none" }),
            $(t).BTQSlider({
              items: 3,
              navigation: !0,
              pagination: !1,
              rewindNav: !1,
              afterInit: function(e) {
                if ($(".number-item.current").length) {
                  var t = $(".number-item.current")
                    .parent()
                    .index();
                  $(e).trigger("BTQ.goTo", t - 1);
                }
              },
              afterAction: function() {
                this.$BTQItems.removeClass("select-page"),
                  this.$BTQItems.eq(this.currentItem).addClass("select-page");
              }
            });
        });
    } else
      $(".number-slider").each(function(e, t) {
        $(t).find(".number-item").length <= 1 &&
          $(t)
            .parent()
            .css({ display: "none" });
      });
  if (
    ($("#business-page, #search-page").length &&
      $(window).width() > 1100 &&
      ($(".slider-member").BTQSlider({
        singleItem: !0,
        navigation: !0,
        pagination: !1,
        slideSpeed: 800,
        rewindNav: !1,
        afterInit: function() {
          $(".slider-member").addClass("fadein"),
            $(".scrollC").scrollTop(0),
            setTimeout(function() {
              ScrollNiceC();
            }, 300);
        },
        afterAction: function() {
          ScrollNiceHide(),
            $(".sub-nav li").removeClass("current"),
            this.$BTQItems.removeClass("show-member prev-show next-show"),
            this.$BTQItems.eq(this.currentItem).addClass("show-member"),
            $(".show-member")
              .prev()
              .addClass("prev-show"),
            $(".show-member")
              .next()
              .addClass("next-show");
          var e = $(".show-member")
              .parent()
              .parent()
              .parent()
              .parent()
              .parent()
              .find(".sub-nav"),
            t = $(".show-member .content-center").attr("data-target");
          $(e)
            .find('li a[data-name= "' + t + '"]')
            .parent()
            .addClass("current"),
            setTimeout(function() {
              ScrollNiceC();
            }, 800);
        }
      }),
      $(".member-center").on("mousewheel", ".slide-wrapper ", function(e) {
        if (e.deltaY > 0) {
          if (!doWheel) return;
          (doWheel = !1),
            $(".slider-member").trigger("BTQ.prev"),
            setTimeout(turnWheelTouch, 500);
        } else {
          if (!doWheel) return;
          (doWheel = !1),
            $(".slider-member").trigger("BTQ.next"),
            setTimeout(turnWheelTouch, 500);
        }
        e.preventDefault();
      })),
    $("#member-page").length && $(window).width() > 1100)
  ) {
    new Swiper(".slider-member", {
      paginationClickable: !0,
      nextButton: ".button-next",
      prevButton: ".button-prev",
      hashnav: !0,
      slidesPerView: 1,
      keyboardControl: !0,
      mousewheelControl: !0,
      simulateTouch: !0,
      effect: "slide",
      parallax: !0,
      speed: 800,
      onInit: function(e) {
        $(".slider-member").addClass("fadein"),
          $(".content-center").removeClass("show-member"),
          $(".content-center")
            .eq(e.activeIndex)
            .addClass("show-member");
        var t = $(".show-member").attr("data-hash");
        $('.sub-nav li a[data-name= "' + t + '"]')
          .parent()
          .addClass("current");
        var i = $(".show-member .scrollC").innerHeight();
        i >= $(window).height() - 270 &&
          ($(".scrollC").scrollTop(0),
          setTimeout(function() {
            ScrollNiceC();
          }, 800));
      },
      onTransitionStart: function() {
        $(".content-center").removeClass("show-member"),
          $(".sub-nav li").removeClass("current"),
          ScrollNiceHide();
      },
      onTransitionEnd: function(e) {
        $(".content-center")
          .eq(e.activeIndex)
          .addClass("show-member");
        var t = $(".show-member .scrollC").innerHeight();
        t >= $(window).height() - 270 &&
          ($(".scrollC").scrollTop(0),
          setTimeout(function() {
            ScrollNiceC();
          }, 800));
        var i = $(".show-member").attr("data-hash");
        $('.sub-nav li a[data-name= "' + i + '"]')
          .parent()
          .addClass("current");
      }
    });
  }
  if ($("#project-page").length && $(window).width() > 1100) {
    var n = new Swiper(".slider-project", {
      speed: 800,
      paginationClickable: !0,
      direction: "vertical",
      slidesPerView: 1,
      keyboardControl: !0,
      mousewheelControl: !0,
      simulateTouch: !0,
      effect: "slide",
      onInit: function(e) {
        $(".content-center").removeClass("ani-pro"),
          $(".content-center")
            .eq(e.activeIndex)
            .addClass("ani-pro");
        var t = $(".ani-pro").attr("data-hash");
        $('.sub-nav li a[data-name= "' + t + '"]')
          .parent()
          .addClass("current");
      },
      onTransitionStart: function() {
        $(".content-center").removeClass("ani-pro"),
          $(".sub-nav li").removeClass("current");
      },
      onTransitionEnd: function(e) {
        $(".content-center")
          .eq(e.activeIndex)
          .addClass("ani-pro");
        var t = $(".ani-pro").attr("data-hash");
        $('.sub-nav li a[data-name= "' + t + '"]')
          .parent()
          .addClass("current"),
          $(".sub-nav li:last-child").hasClass("current")
            ? $(".scroll-down-desktop").removeClass("show")
            : $(".scroll-down-desktop").addClass("show");
      }
    });
    setTimeout(function() {
      n.once("onInit");
    }, 100);
  }
  if (
    ($("#project-details-page").length &&
      ($(window).width() <= 1100 &&
        $(".slide-pro").BTQSlider({
          singleItem: !0,
          navigation: !1,
          pagination: !0,
          autoHeight: !0,
          afterInit: function(e) {
            $(e).trigger("BTQ.goTo", 1);
          },
          afterAction: function() {
            this.$BTQItems.removeClass("select-pro"),
              this.$BTQItems.eq(this.currentItem).addClass("select-pro");
          }
        }),
      $(".grid-pro").BTQSlider({
        singleItem: !0,
        navigation: !1,
        pagination: !0,
        afterInit: function(e) {
          var t = $(e).find(".slide-item").length;
          console.log(t), t >= 3 && $(e).trigger("BTQ.goTo", 1);
        },
        afterAction: function() {
          this.$BTQItems.removeClass("select-thumb"),
            this.$BTQItems.eq(this.currentItem).addClass("select-thumb");
        }
      })),
    $("#resource-page").length && $(window).width() > 1100)
  ) {
    var l = new Swiper(".slider-resource", {
      speed: 800,
      paginationClickable: !0,
      direction: "vertical",
      hashnav: !0,
      slidesPerView: 1,
      keyboardControl: !0,
      mousewheelControl: !0,
      simulateTouch: !0,
      effect: "slide",
      onInit: function(e) {
        $(".slider-resource").addClass("fadein"),
          $(".content-center").removeClass("ani-resource"),
          $(".content-center")
            .eq(e.activeIndex)
            .addClass("ani-resource");
        var t = $(".ani-resource").attr("data-hash");
        $('.sub-nav li a[data-name= "' + t + '"]')
          .parent()
          .trigger("click"),
          ScrollNiceB();
        var i = $(".ani-resource")
          .find(".scrollC")
          .innerHeight();
        i >= $(window).height() - 170 &&
          ($(".scrollC").scrollTop(0),
          setTimeout(function() {
            ScrollNiceC();
          }, 800));
        var o = $(".ani-resource")
          .find(".scrollD")
          .innerHeight();
        o >= $(window).height() - 220 &&
          ($(".scrollD").scrollTop(0),
          setTimeout(function() {
            ScrollNiceD();
          }, 800));
      },
      onTransitionStart: function() {
        $(".content-center").removeClass("ani-resource"), ScrollNiceHide();
      },
      onTransitionEnd: function(e) {
        $(".content-center")
          .eq(e.activeIndex)
          .addClass("ani-resource");
        var t = $(".ani-resource")
          .find(".scrollC")
          .innerHeight();
        t >= $(window).height() - 170 &&
          ($(".scrollC").scrollTop(0),
          setTimeout(function() {
            ScrollNiceC();
          }, 800));
        var i = $(".ani-resource")
          .find(".scrollD")
          .innerHeight();
        i >= $(window).height() - 220 &&
          ($(".scrollD").scrollTop(0),
          setTimeout(function() {
            ScrollNiceD();
          }, 800));
        var o = $(".ani-resource").attr("data-hash");
        $('.sub-nav li a[data-name= "' + o + '"]')
          .parent()
          .trigger("click"),
          $(".sub-nav li:last-child").hasClass("current")
            ? $(".scroll-down-desktop").removeClass("show")
            : $(".scroll-down-desktop").addClass("show");
      }
    });
    setTimeout(function() {
      l.once("onInit");
    }, 100);
  }
  $(".album-box").length &&
    ($(".album-box").BTQSlider({
      singleItem: !0,
      slideSpeed: 800,
      navigation: !0,
      pagination: !0,
      rewindNav: !1,
      autoHeight: !0,
      afterAction: function() {
        this.$BTQItems.removeClass("select-pic"),
          this.$BTQItems.eq(this.currentItem).addClass("select-pic");
      }
    }),
    $(".album-box")
      .parent()
      .on("mousewheel", function(e) {
        if (e.deltaY > 0) {
          if (!doWheel) return;
          (doWheel = !1),
            $(".album-box").trigger("BTQ.prev"),
            setTimeout(turnWheelTouch, 500);
        } else {
          if (!doWheel) return;
          (doWheel = !1),
            $(".album-box").trigger("BTQ.next"),
            setTimeout(turnWheelTouch, 500);
        }
        e.preventDefault();
      })),
    $(".video-box").length &&
      ($(".video-box").BTQSlider({
        singleItem: !0,
        slideSpeed: 800,
        navigation: !0,
        pagination: !0,
        rewindNav: !1,
        autoHeight: !0,
        afterAction: function() {
          this.$BTQItems.removeClass("select-pic"),
            this.$BTQItems.eq(this.currentItem).addClass("select-pic");
        }
      }),
      $(".video-box")
        .parent()
        .on("mousewheel", function(e) {
          if (e.deltaY > 0) {
            if (!doWheel) return;
            (doWheel = !1),
              $(".video-box").trigger("BTQ.prev"),
              setTimeout(turnWheelTouch, 500);
          } else {
            if (!doWheel) return;
            (doWheel = !1),
              $(".video-box").trigger("BTQ.next"),
              setTimeout(turnWheelTouch, 500);
          }
          e.preventDefault();
        }));
}
function StopTime() {
  timex > 0 && (clearTimeout(timex), (timex = 0));
}
function addMove() {
  $(".left-content").removeClass("move"),
    $(".left-content h3")
      .children()
      .removeClass("move"),
    $(".box-text, .border").removeClass("show"),
    $(".show-text .left-content").addClass("move");
  var e = $(".show-text .left-content h3").children().length,
    t = 200 * e;
  setTimeout(function() {
    $(".show-text .box-text").addClass("show");
  }, t),
    setTimeout(function() {
      $(".show-text .box-text .border").addClass("show");
    }, t),
    $(".move h3")
      .children()
      .each(function(e) {
        var t = $(this);
        timex = setTimeout(function() {
          $(t).addClass("move");
        }, 200 * (e + 1));
      });
}
function AniText() {
  $(".title-page").addClass("show"),
    $(".title-page h1")
      .children()
      .children()
      .each(function(e) {
        var t = $(this);
        setTimeout(function() {
          $(t).addClass("move");
        }, 100 * (e + 1));
      });
}
function SustainableLoad(e, t, i) {
  $.ajax({
    url: e,
    cache: !1,
    success: function(e) {
      $(t).html(e),
        $(window).width() > 1100 &&
          $(".ani-sus .content-sustainable").each(function(e, t) {
            var i = $(t)
              .find(".scrollC")
              .innerHeight();
            i >= $(window).height() - 170
              ? $(t)
                  .find(".scrollC")
                  .css({ height: $(window).height() - 170 })
              : $(t)
                  .find(".scrollC")
                  .css({ height: "auto" });
          }),
        $(i)
          .removeClass("flipoutx_1")
          .addClass("fadeinup_1"),
        $(t).animate({ opacity: 1 }, 300, "linear", function() {
          $(".loadicon").fadeOut(300, "linear", function() {
            if (
              ($(".container").removeClass("dim-light"),
              $(window).width() > 1100)
            )
              $(".ani-sus .content-sustainable").each(function(e, t) {
                var i = $(t)
                  .find(".scrollC")
                  .innerHeight();
                i >= $(window).height() - 170
                  ? ($(t)
                      .find(".scrollC")
                      .css({ height: $(window).height() - 170 }),
                    setTimeout(ScrollNiceC, 300))
                  : $(t)
                      .find(".scrollC")
                      .css({ height: "auto" });
              });
            else {
              var e = $(i).offset().top;
              $("html, body").animate({ scrollTop: e - 50 }, "slow"),
                detectBut();
            }
            $(".loadicon").remove();
          });
        });
    }
  });
}
function NewsLoad(e, t) {
  $.ajax({
    url: e,
    cache: !1,
    success: function(e) {
      $(t)
        .find(".news-content")
        .append(e),
        $(".news-text img").addClass("zoom-pic"),
        ZoomPic(),
        $(".news-text a, .news-text p a").click(function(e) {
          e.preventDefault();
          var t = $(this).attr("href");
          return window.open(t, "_blank"), !1;
        }),
        $(t)
          .find(".news-content")
          .stop()
          .animate({ opacity: 1 }, 100, "linear", function() {
            if (
              ($(".news-list").removeClass("no-link"), $(window).width() > 1100)
            ) {
              ScrollNiceD();
              var e = $(t)
                  .find(".link-page.current")
                  .offset().top,
                i = $(t)
                  .find(".news-list")
                  .offset().top;
              0 == News &&
                ($(t)
                  .find(".scrollB")
                  .stop()
                  .animate({ scrollTop: e - i }),
                (News = 1));
            } else {
              var o = $(".colum-box.active").innerHeight();
              $(".box-content").css({ height: o }),
                $(".news-text").imagesLoaded(function() {
                  var e = $(".colum-box.active").innerHeight();
                  $(".box-content").css({ height: e });
                }),
                detectBut();
            }
            $(".news-text").addClass("fadein"),
              $(".loadicon2").fadeOut(300, "linear", function() {
                $(".loadicon2, .loadicon").remove();
              });
          });
    }
  });
}
function NewsProjectLoad(e, t) {
  $.ajax({
    url: e,
    cache: !1,
    success: function(e) {
      $(t)
        .find(".news-content")
        .append(e),
        $(".news-text img").addClass("zoom-pic"),
        ZoomPic(),
        $(".news-text a, .news-text p a").click(function(e) {
          e.preventDefault();
          var t = $(this).attr("href");
          return window.open(t, "_blank"), !1;
        }),
        $("div .newsload").removeClass("newsload"),
        $(t)
          .find(".colum-box-news-project")
          .addClass("show"),
        $(t)
          .find(".news-content")
          .stop()
          .animate({ opacity: 1 }, 100, "linear", function() {
            if (
              ($(".news-list").removeClass("no-link"), $(window).width() > 1100)
            ) {
              ScrollNiceD();
              var e = $(t)
                  .find(".link-page.current")
                  .offset().top,
                i = $(t)
                  .find(".news-list-project")
                  .offset().top;
              0 == News &&
                ($(t)
                  .find(".scrollG")
                  .stop()
                  .animate({ scrollTop: e - i }),
                (News = 1));
            } else {
              var o =
                $(t)
                  .find(".news-text")
                  .offset().top - 60;
              $("html, body").animate({ scrollTop: o }, "slow");
            }
            $(".news-text").addClass("fadein"),
              $(".loadicon").fadeOut(300, "linear", function() {
                $(".loadicon").remove();
              });
          }),
        $(".close").click(function(e) {
          e.preventDefault(),
            $(this)
              .parent()
              .addClass("exit");
          var t = $(this)
            .parent()
            .parent()
            .offset().top;
          return (
            $("html, body").scrollTop(t),
            $(this)
              .parent()
              .parent()
              .find(".link-page.current")
              .removeClass("current"),
            $(".exit")
              .find(".news-content")
              .stop()
              .animate({ opacity: 0 }, 300, "linear", function() {
                $(".exit")
                  .find(".news-content")
                  .children()
                  .remove(),
                  $(".exit").removeClass("show"),
                  $(".colum-box-news-project.exit").removeClass("exit");
              }),
            !1
          );
        });
    }
  });
}

function VideoLoad(e) {
  $.ajax({
    url: e,
    cache: !1,
    success: function(e) {
      function t() {
        o.play();
      }
      function i() {
        o.pause();
      }
      $(".allvideo").append(e);
      var o = document.getElementById("view-video");
      if ($(window).width() > 1100) {
        var a = 0.5625,
          n = $(window).height() - 100,
          l = n / a;
        $(".video-wrap").css({
          width: l,
          height: n,
          "margin-left": -l / 2,
          "margin-top": -n / 2 + 30
        });
      } else
        $(".video-wrap").css({ width: "100%", height: "100%", margin: 0 }),
          $("#view-video").on("click touchstart", function() {
            this.paused && this.play();
          }),
          setTimeout(function() {
            $("#view-video").trigger("touchstart");
          }, 300);
      $(".loadicon").fadeOut(300, "linear", function() {
        t(), $(".loadicon").remove();
      });
      var r = $("#view-video").length;
      $(".close-video").click(function() {
        0 != r && i(),
          $(".video-list, .video-skin").fadeOut(500, "linear", function() {
            if (
              ($(".allvideo").removeClass("show"),
              $(".library-load").length &&
                $(window).width() > 1100 &&
                $(".library-center").trigger("BTQ.play", 5e3),
              $(".overlay-video").fadeOut(500, "linear", function() {
                $(".allvideo").css({ display: "none" }),
                  $(".close-video").fadeOut(300, "linear", function() {
                    $(".allvideo .video-list").remove();
                  });
              }),
              $("html, body").removeClass("no-scroll"),
              $(".to-scrollV").length)
            ) {
              var e = $(".to-scrollV").offset().top;
              $(".to-scrollV").removeClass("to-scrollV"),
                $(window).width() < 1100 && $("html, body").scrollTop(e - 60);
            }
          });
      });
    }
  });
}
function AlbumLoad(e) {
  $.ajax({
    url: e,
    cache: !1,
    success: function(e) {
      function t() {
        $(".show-active:first-child").hasClass("show-active")
          ? $(".prev-pic").addClass("disable")
          : $(".prev-pic").removeClass("disable"),
          $(".show-active:last-child").hasClass("show-active")
            ? $(".next-pic").addClass("disable")
            : $(".next-pic").removeClass("disable");
      }
      function i() {
        clearTimeout(timex),
          $(".pic-name").each(function() {
            var e = $(this)
              .find("h3")
              .text();
            e.length > 1
              ? $(this)
                  .find("h3")
                  .css({ display: "inline-block" })
              : $(this)
                  .find("h3")
                  .css({ display: "none" });
          }),
          $(".pic-name").removeClass("move"),
          $(".pic-name h3")
            .children()
            .children()
            .removeClass("move"),
          $(".slide-item.show-active")
            .find(".pic-name")
            .addClass("move"),
          $(".move h3")
            .children()
            .children()
            .each(function(e) {
              var t = $(this);
              setTimeout(function() {
                $(t).addClass("move");
              }, 50 * (e + 1));
            });
      }
      $(".all-album").append(e),
        $(".album-pic-center").css({ height: $(window).height() }),
        $(".pic-name > h3")
          .lettering("words")
          .children("span")
          .lettering()
          .children("span")
          .lettering(),
        $(".album-center").BTQSlider({
          singleItem: !0,
          pagination: !1,
          rewindNav: !1,
          lazyLoad: !0,
          mouseDrag: !0,
          slideSpeed: 600,
          paginationSpeed: 600,
          afterAction: function() {
            this.$BTQItems.removeClass("show-active"),
              this.$BTQItems.eq(this.currentItem).addClass("show-active"),
              t(),
              i();
          }
        }),
        $(".album-center").on("mousewheel", ".slide-wrapper ", function(e) {
          if (e.deltaY > 0) {
            if (!doWheel) return;
            (doWheel = !1),
              $(".album-center").trigger("BTQ.prev"),
              setTimeout(turnWheelTouch, 500);
          } else {
            if (!doWheel) return;
            (doWheel = !1),
              $(".album-center").trigger("BTQ.next"),
              setTimeout(turnWheelTouch, 500);
          }
          e.preventDefault();
        }),
        $(".next-pic").click(function() {
          $(".album-center").trigger("BTQ.next");
        }),
        $(".prev-pic").click(function() {
          $(".album-center").trigger("BTQ.prev");
        }),
        $(".all-album")
          .stop()
          .animate({ opacity: 1 }, 100, "linear", function() {
            $(".album-pic-center").length > 1 &&
              $(".slide-pic-nav").css({ display: "block" });
          }),
        $(".album-load").fadeIn(800, "linear", function() {
          $(".loadicon").fadeOut(300, "linear", function() {
            $(".loadicon").remove();
          });
        }),
        $(".album-pic-center img").addClass("zoom-pic"),
        ZoomPic(),
        $(".close-album").click(function() {
          if (
            ($(".all-album").fadeOut(500, "linear", function() {
              $(".album-load").remove();
            }),
            $(".overlay-album").animate(
              { height: "0%" },
              600,
              "easeOutExpo",
              function() {
                $(".overlay-album").css({ display: "none" });
              }
            ),
            $("html, body").removeClass("no-scroll"),
            $(".to-scrollAB").length)
          ) {
            var e = $(".to-scrollAB").offset().top;
            $("to-scrollAB").removeClass("to-scrollAB"),
              $(window).width() < 1100 && $("html, body").scrollTop(e - 60);
          }
          return !1;
        });
    }
  });
}
function FocusText() {
  var e =
      "Họ và Tên (*)  Điện thoại (*) Email (*) Nội dung cần liên hệ (*) Tìm kiếm... Người gửi Địa chỉ email Điện thoại gửi đến nội dung Email... Full name (*) Phone (*) Address Search... ",
    t = "";
  $("input").focus(function() {
    (t = $(this).val()), e.indexOf(t) >= 0 && $(this).val("");
  }),
    $("input").focusout(function() {
      "" == $(this).val() && $(this).val(t);
    });
  var i = "";
  $("textarea")
    .focus(function() {
      (i = $(this).val()), $(this).val("");
    })
    .focusout(function() {
      "" == $(this).val() && $(this).val(i);
    });
}
function ScrollHoz() {
  var e = $(
    ".news-list, .sub-news, .content-table, .sub-menu, .sub-nav-typical, .number-box"
  );
  $(window).width() <= 1100 &&
    ($("#news-page").length &&
      $(".scrollB").each(function() {
        var e = $(this).children().length,
          t =
            $(this)
              .children()
              .width() + 5;
        $(this).width(e * t);
      }),
    $(e).css({
      "overflow-x": "scroll",
      "overflow-y": "hidden",
      "-ms-touch-action": "auto",
      "-ms-overflow-style": "none",
      overflow: " -moz-scrollbars-none",
      "-webkit-overflow-scrolling": "touch"
    }),
    $(e).animate({ scrollLeft: "0px" }),
    (0 != TouchLenght && isTouchDevice) ||
      ($(window).width() <= 1100 &&
        ($(e).mousewheel(function(e, t) {
          e.preventDefault(),
            $(window).width() <= 1100 && (this.scrollLeft -= 40 * t);
        }),
        $(e).addClass("dragscroll"),
        $(".dragscroll").draptouch())));
}
function ScrollNiceA() {
  $(window).width() <= 1100
    ? ($(".scrollA")
        .getNiceScroll()
        .remove(),
      $(".scrollA").css({ "overflow-x": "visible", "overflow-y": "visible" }))
    : ($(".ani-text .scrollA").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
      }),
      $(".ani-text .scrollA")
        .getNiceScroll()
        .show(),
      $(".ani-text .scrollA").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }),
      $(".ani-text .scrollA").scrollTop(0));
}
function ScrollNiceB() {
  $(window).width() <= 1100
    ? ScrollHoz()
    : ($(".scrollB").css({ "overflow-x": "hidden", "overflow-y": "hidden" }),
      $(".scrollB")
        .getNiceScroll()
        .show(),
      $(".scrollB").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursorcolor: "#ffffff",
        cursoropacitymin: 1
      }),
      $(".scrollB").scrollTop(0));
}
function ScrollNiceC() {
  $(window).width() <= 1100
    ? ($(".scrollC")
        .getNiceScroll()
        .remove(),
      $(".scrollC").css({ "overflow-x": "visible", "overflow-y": "visible" }))
    : ($(".scrollC").css({ "overflow-x": "hidden", "overflow-y": "hidden" }),
      $(".select-history .scrollC")
        .getNiceScroll()
        .show(),
      $(".select-history .scrollC").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 0
      }),
      $(".select-history .scrollC").scrollTop(0),
      $(".development .scrollC")
        .getNiceScroll()
        .show(),
      $(".development .scrollC").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }),
      $(".development .scrollC").scrollTop(0),
      $(".show-member .scrollC")
        .getNiceScroll()
        .show(),
      $(".show-member .scrollC").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }),
      $(".show-member .scrollC")
        .getNiceScroll()
        .show(),
      $(".show-member .scrollC").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }),
      $(".ani-sus .scrollC")
        .getNiceScroll()
        .show(),
      $(".ani-sus .scrollC").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }),
      $(".ani-resource .scrollC")
        .getNiceScroll()
        .show(),
      $(".ani-resource .scrollC").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }));
}
function ScrollNiceD() {
  $(window).width() <= 1100
    ? ($(".scrollD")
        .getNiceScroll()
        .remove(),
      $(".scrollD").css({ "overflow-x": "visible", "overflow-y": "visible" }))
    : ($(".scrollD").css({ "overflow-x": "hidden", "overflow-y": "hidden" }),
      $(".scrollD")
        .getNiceScroll()
        .show(),
      $(".scrollD").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }),
      $(".scrollD").scrollTop(0));
}
function ScrollNiceE() {
  $(window).width() <= 1100
    ? ($(".scrollE")
        .getNiceScroll()
        .remove(),
      $(".scrollE").css({ "overflow-x": "visible", "overflow-y": "visible" }))
    : ($(".active .scrollE").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
      }),
      $(".active .scrollE")
        .getNiceScroll()
        .show(),
      $(".active .scrollE").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }),
      $(".active .scrollE").scrollTop(0));
}
function ScrollNiceF() {
  $(window).width() <= 1100
    ? ($(".scrollF")
        .getNiceScroll()
        .remove(),
      $(".scrollF").css({ "overflow-x": "visible", "overflow-y": "visible" }))
    : ($(".active .scrollF").css({
        "overflow-x": "hidden",
        "overflow-y": "hidden"
      }),
      $(".active .scrollF")
        .getNiceScroll()
        .show(),
      $(".active .scrollF").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !0,
        cursoropacitymin: 1
      }),
      $(".active .scrollF").scrollTop(0));
}
function ScrollNiceG() {
  $(window).width() <= 1100
    ? ($(".scrollG")
        .getNiceScroll()
        .remove(),
      $(".scrollG").css({ "overflow-x": "visible", "overflow-y": "visible" }))
    : ($(".scrollG").css({ "overflow-x": "hidden", "overflow-y": "hidden" }),
      $(".scrollG")
        .getNiceScroll()
        .show(),
      $(".scrollG").niceScroll({
        touchbehavior: !0,
        horizrailenabled: !1,
        cursordragontouch: !0,
        grabcursorenabled: !1,
        cursorcolor: "#ffffff",
        cursoropacitymin: 1
      }),
      $(".scrollG").scrollTop(0));
}
function ScrollNiceHide() {
  $(".scrollA, .scrollB, .scrollC, .scrollD, .scrollE, .scrollF, .scrollG")
    .getNiceScroll()
    .remove();
}
function LinkPage() {
  $(
    "a.link-load, a.link-home, a.go-page, a.go-details,  a.go-back, a.house,.go-home, .link-icon a, .project-view"
  ).click(function(e) {
    return (
      e.preventDefault(),
      (linkLocation = $(this).attr("href")),
      (window.location = linkLocation),
      ScrollNiceHide(),
      $(".overlay-menu").trigger("click"),
      $(".close-video, .close-box").trigger("click"),
      $(".footer, .go-top, .map, .scroll-down-desktop, .container")
        .removeClass("fadeinup")
        .removeClass("fadein")
        .removeClass("show")
        .addClass("fadeout"),
      $("html, body").addClass("no-scroll"),
      !1
    );
  });
}
function ContentLoad() {
  if (
    (ResizeWindows(),
    detectHeight(),
    LinkPage(),
    FocusText(),
    NavClick(),
    SlidePicture(),
    Search(),
    Search1(),
    Option(),
    ScrollHoz(),
    $("html, body").removeClass("no-scroll"),
    $("#home-page").length ||
      ($(".logo").css({ cursor: "pointer" }),
      $(".logo").click(function() {
        $("a.link-home").trigger("click");
      })),
    $(".sub-nav, .sub-news, .top-project").addClass("show"),
    setTimeout(function() {
      AniText();
    }, 300),
    setTimeout(function() {
      $(".footer").addClass("fadeinup");
    }, 500),
    setTimeout(function() {
      $(".next-prev, .go-back").addClass("fadeinup");
    }, 800),
    setTimeout(function() {
      $(".sub-nav li:last-child").hasClass("current") ||
        $(".scroll-down-desktop").addClass("show");
    }, 800),
    $("#home-page").length &&
      ($(".link-home").addClass("current"),
      $(window).width() > 1100 &&
        $(".popup-pics img").length > 0 &&
        ($(".overlay-dark").fadeIn(500, "linear", function() {
          $(".popup-pics").fadeIn(500, "linear"),
            $("body").removeClass("first-time");
        }),
        $(".close-popup, .overlay-dark").click(function() {
          return (
            $(".popup-pics, .overlay-dark").fadeOut(
              500,
              "linear",
              function() {}
            ),
            !1
          );
        })),
      $(".box:not(.video-home)").click(function(e) {
        e.preventDefault(),
          $(this)
            .find(".go-details")
            .trigger("click");
      }),
      $(".box.video-home").click(function(e) {
        e.preventDefault(),
          $(this)
            .find(".player")
            .trigger("click");
      })),
    $("#about-page").length &&
      ($(".sub-nav li").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li").removeClass("current"),
          $(this).addClass("current");
        var t = $(this)
          .find("a")
          .attr("data-name");
        if ($(window).width() > 1100) {
          var i = $(".slider-about")[0].swiper,
            o = $(".content-center[data-hash='" + t + "']").index();
          i.slideTo(o, 1e3, !0);
        }
        return !1;
      }),
      $(".history-sub li").click(function(e) {
        e.preventDefault(),
          $(".history-sub li").removeClass("current"),
          $(this).addClass("current");
        var t = $(this)
          .find("a")
          .attr("data-year");
        if ($(window).width() > 1100) {
          var i = $(".history-slide")[0].swiper,
            o = $(".year[data-target='" + t + "']").index();
          i.slideTo(o, 1e3, !0);
        }
        return !1;
      }),
      $(".cycle li a").mouseenter(function() {
        if ($(window).width() > 1100) {
          $(".box-info").removeClass("show");
          var e = $(this).attr("data-show");
          $(".box-info[data-target='" + e + "']").addClass("show");
        }
        return !1;
      }),
      $(".cycle li a").mouseleave(function() {
        return (
          $(window).width() > 1100 && $(".box-info").removeClass("show"), !1
        );
      }),
      $(".scroll-down-desktop").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li.current")
            .next()
            .find("a")
            .trigger("click");
      }),
      $(window).width() > 1100
        ? $(".sub-nav li.current").length
          ? $(".sub-nav li.current").trigger("click")
          : $(".sub-nav li:first-child").trigger("click")
        : $(".slider-about").addClass("fadein")),
    $("#member-page").length &&
      ($(".sub-nav li").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li").removeClass("current"),
          $(this).addClass("current");
        var t = $(this)
          .find("a")
          .attr("data-name");
        if ($(window).width() > 1100) {
          var i = $(".slider-member")[0].swiper,
            o = $(".content-center[data-hash='" + t + "']").index();
          i.slideTo(o, 1e3, !0);
        }
        return !1;
      }),
      $(window).width() > 1100
        ? $(".sub-nav li.current").length
          ? $(".sub-nav li.current").trigger("click")
          : $(".sub-nav li:first-child").trigger("click")
        : $(".slider-member").addClass("fadein")),
    $("#business-page").length &&
      ($(".sub-nav li").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li").removeClass("current"),
          $(this).addClass("current");

        return !1;
      }),
      $(window).width() > 1100
        ? $(".sub-nav li.item-current").length
          ? setTimeout(function() {
              $(".sub-nav li.item-current").trigger("click");
            }, 1500)
          : $(".sub-nav li:first-child").trigger("click")
        : $(".slider-member").addClass("fadein")),
    $("#project-page").length &&
      ($(".sub-nav li").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li").removeClass("current"),
          $(this).addClass("current");
        var t = $(this)
          .find("a")
          .attr("data-name");
        if ($(window).width() > 1100) {
          var i = $(".slider-project")[0].swiper,
            o = $(".content-center[data-hash='" + t + "']").index();
          i.slideTo(o, 1e3, !0);
        }
        return !1;
      }),
      $(".scroll-down-desktop").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li.current")
            .next()
            .find("a")
            .trigger("click");
      }),
      $(".slider-project").addClass("fadein")),
    $("#project-details-page").length &&
      ($('.nav li a[data-name = "project-page"]')
        .parent()
        .addClass("active"),
      $(".project-element").addClass("show"),
      $(
        ".pic-project:not(.small-library, .small-facilities) img, .bg-project img"
      ).addClass("zoom-pic"),
      ZoomPic(),
      $(".link-page a").click(function(e) {
        e.preventDefault(),
          $(".loadicon").length ||
            $("body").append(
              '<div class="loadicon" style="display:block"></div>'
            );
        var t = $(this)
          .parent()
          .parent()
          .parent()
          .parent();
        $(this)
          .parent()
          .parent()
          .find(".link-page")
          .removeClass("current"),
          $(this)
            .parent()
            .addClass("current");
        var i = ($(this).attr("data-name"), $(this).attr("href"));
        return (
          $(t)
            .find(".news-content")
            .addClass("newsload"),
          $(t)
            .find(".news-content")
            .stop()
            .animate({ opacity: 0 }, 600, "linear", function() {
              $(".newsload").children().length &&
                ($(".scrollD")
                  .getNiceScroll()
                  .remove(),
                $(".newsload")
                  .children()
                  .remove(),
                $("div .newsload").removeClass("newsload")),
                NewsProjectLoad(i, t);
            }),
          !1
        );
      }),
      $(".sub-menu li a").click(function(e) {
        if ((e.preventDefault(), ScrollNiceHide(), $(window).width() > 1100)) {
          var t = $(".colum-project").length,
            i = $(".colum-project").width();
          $(".box-content-project").width(t * i),
            $(".sub-menu li").removeClass("current"),
            $(".colum-project").removeClass("active"),
            $(this)
              .parent()
              .addClass("current");

          var c = $(".box-content-project").offset().left,
            d = $(
              '.box-content-project .colum-project[data-hash= "' + o + '"]'
            ).offset().left;
          $('.colum-project[data-hash= "' + o + '"]').addClass("active");
          var u = $(".colum-project.active")
            .find(".news-content")
            .children();
          u.length <= 0 &&
            $(".colum-project.active")
              .find(".link-page:first-child a")
              .trigger("click"),
            detectBut(),
            $(".box-content-project")
              .stop()
              .animate({ left: c - d }, 800, "easeInOutExpo", function() {
                $(".colum-project.active ")
                  .find(".content-pro-text")
                  .addClass("fadein"),
                  $(".colum-project.active ")
                    .find(".bg-project")
                    .addClass("fadeinup"),
                  $(".colum-project.active")
                    .find(".colum-box-news-project")
                    .addClass("fadeinup"),
                  $(".colum-project.active")
                    .find(".news-list-project")
                    .addClass("show"),
                  $(".colum-project.active .pic-project").each(function(e) {
                    var t = $(this);
                    setTimeout(function() {
                      $(t).addClass("fadeinup");
                    }, 200 * (e + 1));
                  }),
                  $(".colum-project.active .pic-product").each(function(e) {
                    var t = $(this);
                    setTimeout(function() {
                      $(t).addClass("fadeinup");
                    }, 200 * (e + 1));
                  }),
                  $(".colum-project.active .brochure-box").each(function(e) {
                    var t = $(this);
                    setTimeout(function() {
                      $(t).addClass("fadeinup");
                    }, 200 * (e + 1));
                  }),
                  $(".colum-project.active .scrollD").length &&
                    setTimeout(function() {
                      ScrollNiceD();
                    }, 150),
                  $(".colum-project.active .scrollE").length &&
                    setTimeout(function() {
                      ScrollNiceE();
                    }, 150),
                  $(".colum-project.active .scrollF").length &&
                    setTimeout(function() {
                      ScrollNiceF();
                    }, 150),
                  $(".colum-project.active .scrollG").length &&
                    setTimeout(function() {
                      ScrollNiceG();
                    }, 150),
                  $(window).width() > 1100 &&
                    $(".bg-project img").imagePanning();
              });
        }
        return !1;
      }),
      $(".prevslide").click(function(e) {
        e.preventDefault(),
          doWheel &&
            ((doWheel = !1),
            $(".sub-menu li.current")
              .prev()
              .find("a")
              .trigger("click"),
            setTimeout(turnWheelTouch, 800));
      }),
      $(".nextslide").click(function(e) {
        e.preventDefault(),
          doWheel &&
            ((doWheel = !1),
            $(".sub-menu li.current")
              .next()
              .find("a")
              .trigger("click"),
            setTimeout(turnWheelTouch, 800));
      }),
      $(window).width() > 1100 &&
        ($(".link-page:first-child a").trigger("click"),
        $(".sub-menu li.current").length
          ? $(".sub-menu li.current a").trigger("click")
          : $(".sub-menu li:first-child a").trigger("click"))),
    $("#sustainable-page").length &&
      ($(".sub-nav li").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li").removeClass("current"),
          $(this).addClass("current");
        var t = $(this)
          .find("a")
          .attr("data-name");
        if ($(window).width() > 1100) {
          var i = $(".slider-sustainable")[0].swiper,
            o = $(".content-center[data-hash='" + t + "']").index();
          i.slideTo(o, 1e3, !0);
        }
        return !1;
      }),
      $(".number-item").click(function(e) {
        e.preventDefault();
        var t = $(this).attr("data-ref"),
          i =
            $(this)
              .parent()
              .index() - 1;
        if (
          ($(".loadicon").length ||
            $("body").append(
              '<div class="loadicon" style="display:block"></div>'
            ),
          $(".container").addClass("dim-light"),
          $(window).width() > 1100)
        ) {
          $(this)
            .parent()
            .parent()
            .parent()
            .parent()
            .find(".number-item")
            .removeClass("current"),
            $(this).addClass("current"),
            $(this)
              .parent()
              .parent()
              .parent()
              .parent()
              .trigger("BTQ.goTo", i);
          var o = $(this)
              .parent()
              .parent()
              .parent()
              .parent()
              .parent()
              .parent()
              .find(".scrollC"),
            a = $(this)
              .parent()
              .parent()
              .parent()
              .parent()
              .parent()
              .parent()
              .find(".content-sustainable");
          $(o).height("auto"),
            $(o)
              .getNiceScroll()
              .remove();
        } else {
          $(this)
            .parent()
            .find(".number-item")
            .removeClass("current"),
            $(this).addClass("current");
          var o = $(this)
              .parent()
              .parent()
              .parent()
              .find(".scrollC"),
            a = $(this)
              .parent()
              .parent()
              .parent()
              .find(".content-sustainable");
        }
        return (
          $(a)
            .removeClass("fadeinup_1")
            .addClass("flipoutx_1"),
          $(o)
            .stop()
            .animate({ opacity: 0 }, 300, "linear", function() {
              $(o).children().length &&
                $(o)
                  .children()
                  .remove(),
                SustainableLoad(t, o, a);
            }),
          !1
        );
      }),
      $(".scroll-down-desktop").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li.current")
            .next()
            .find("a")
            .trigger("click");
      }),
      $(window).width() > 1100
        ? $(".sub-nav li.current").length
          ? $(".sub-nav li.current").trigger("click")
          : $(".sub-nav li:first-child").trigger("click")
        : $(".slider-sustainable").addClass("fadein")),
    $("#resource-page").length &&
      ($(".sub-nav li").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li").removeClass("current"),
          $(this).addClass("current");
        var t = $(this)
          .find("a")
          .attr("data-name");
        if (
          (setTimeout(function() {
            if ($(".item-career.item-active").length) {
              var e = $(".colum-box-news .recruitment-content").children()
                .length;
              e || $(".link-page:nth-child(1) .link-career").trigger("click");
            }
          }, 1500),
          $(window).width() > 1100)
        ) {
          var i = $(".slider-resource")[0].swiper,
            o = $(".content-center[data-hash='" + t + "']").index();
          i.slideTo(o, 1e3, !0);
        }
        return !1;
      }),

      $(".scroll-down-desktop").click(function(e) {
        e.preventDefault(),
          $(".sub-nav li.current")
            .next()
            .find("a")
            .trigger("click");
      }),
      $(window).width() > 1100
        ? $(".sub-nav li.current").length
          ? $(".sub-nav li.current").trigger("click")
          : $(".sub-nav li:first-child").trigger("click")
        : $(".slider-resource").addClass("fadein"),
      $(window).width() <= 1100
        ? $(".link-career.current").length
          ? setTimeout(function() {
              $(".link-career.current").trigger("click");
            }, 50)
          : $(".link-page:first-child a.link-career").trigger("click")
        : $(".link-career.current").length &&
          setTimeout(function() {
            $(".link-career.current").trigger("click");
          }, 50)),
    $("#news-page").length &&
      ($(".link-page a").click(function(e) {
        e.preventDefault(),
          $(".loadicon, loadicon2").length ||
            $(".colum-box-news").append(
              '<div class="loadicon2" style="display:block"></div>'
            ),
          $(".news-list").addClass("no-link");
        var t = $(this)
          .parent()
          .parent()
          .parent()
          .parent()
          .parent();
        $(this)
          .parent()
          .parent()
          .find(".link-page")
          .removeClass("current"),
          $(this)
            .parent()
            .addClass("current");

        var r = $(this).attr("href");
        return (
          $(t)
            .find(".news-content")
            .addClass("newsload"),
          $(t)
            .find(".news-content")
            .stop()
            .animate({ opacity: 0 }, 300, "linear", function() {
              $(".scrollD")
                .getNiceScroll()
                .remove(),
                $(".newsload")
                  .children()
                  .remove(),
                $("div .newsload").removeClass("newsload"),
                NewsLoad(r, t),
                (Details = 1);
            }),
          !1
        );
      }),
      $(".sub-news li").click(function(e) {
        e.preventDefault();
        var t = $(".colum-box").length,
          i = $(".colum-box").width();
        $(".box-content").width(t * i),
          $(".sub-news li").removeClass("current"),
          $(".colum-box").removeClass("active"),
          $(this).addClass("current");

        var c = $(".box-content").offset().left,
          d = $('.box-content .colum-box[data-hash= "' + o + '"]').offset()
            .left;
        return (
          $('.colum-box[data-hash= "' + o + '"]').addClass("active"),
          $(".box-content")
            .stop()
            .animate({ left: c - d }, 600, "easeInOutExpo", function() {
              if ("video" == o || "pictures" == o);
              else {
                var e = $(".active")
                  .find(".news-content")
                  .children();
                if ($(e).length) {

                } else
                  $(".active .link-page").hasClass("current")
                    ? $(".active .link-page.current a").trigger("click")
                    : $(".active .link-page:first-child a").trigger("click");
              }
              if ($(window).width() > 1100)
                $(".box-content, .colum-box.active").css({
                  height: $(window).height()
                }),
                  $(".colum-box.active")
                    .find(".colum-box-news")
                    .addClass("fadeinup"),
                  $(".colum-box.active")
                    .find(".content-album .album-box")
                    .addClass("fadeinup"),
                  $(".colum-box.active")
                    .find(".content-album .video-box")
                    .addClass("fadeinup"),
                  setTimeout(function() {
                    $(".colum-box.active")
                      .find(".news-list")
                      .addClass("showup");
                  }, 300);
              else {
                var r = $(".colum-box.active").innerHeight();
                $(".box-content").css({ height: r }), detectBut();
              }
            }),
          !1
        );
      }),
      ScrollNiceB(),
      $(".sub-news li.current").length
        ? $(".sub-news li.current").trigger("click")
        : $(".sub-news li:first-child").trigger("click")),
    $("#contact-page").length &&
      (initialize(),
      setTimeout(function() {
        $(".contact-box").addClass("fadeinup");
      }, 300),
      setTimeout(function() {
        $(".contact-form").addClass("fadeinup");
      }, 600)),
    $("#search-page").length)
  )
    if ($(window).width() > 1100) {
      var e = $(".content-center[data-target='s-1']")
        .parent()
        .index();
      $(".slider-member").trigger("BTQ.goTo", e);
    } else $(".slider-member").addClass("fadein");
}
function ZoomPic() {
  $("img").click(function() {
    if ($(window).width() <= 740 && $(this).hasClass("zoom-pic")) {
      $("html, body").addClass("no-scroll"),
        $(this)
          .parent()
          .addClass("to-scrollZ"),
        $(".loadicon").length ||
          $("body").append(
            '<div class="loadicon" style="display:block"></div>'
          ),
        $(".all-pics").css({ display: "block" }),
        $(".all-pics").append(
          '<div class="full"  style="display:block"></div>'
        ),
        $(".overlay-dark").fadeIn(300, "linear");
      var e = $(this).attr("src");
      $(".all-pics")
        .find(".full")
        .append('<img src ="' + e + '" alt="pic" />'),
        $(".all-pics").append('<div class="close-pics-small"></div>'),
        $(".all-pics img").load(function() {
          $(".all-pics").addClass("show"),
            0 != TouchLenght && isTouchDevice
              ? ($(".full").addClass("pinch-zoom"),
                $(".pinch-zoom").each(function() {
                  new Pic.PinchZoom($(this), {});
                }))
              : ($(".full").addClass("dragscroll"),
                $(".dragscroll").draptouch()),
            $(".full img").length > 1 &&
              $(".full img")
                .last()
                .remove(),
            $(".loadicon").fadeOut(400, "linear", function() {
              (0 != TouchLenght && isTouchDevice) || detectMargin(),
                $(".full img").addClass("fadein"),
                $(".loadicon").remove();
            });
        }),
        $(".close-pics-small, .overlay-dark").click(function() {
          $(".loadicon").remove(),
            $(".full, .close-pics-small, .overlay-dark").fadeOut(
              300,
              "linear",
              function() {
                if (
                  ($(
                    ".all-pics .full,  .all-pics .pinch-zoom-container"
                  ).remove(),
                  $(".close-pics-small").remove(),
                  $(".all-pics")
                    .css({ display: "none" })
                    .removeClass("show"),
                  $(".album-pic-center").length ||
                    $("html, body").removeClass("no-scroll"),
                  $(".to-scrollZ").length)
                ) {
                  var e = $(".to-scrollZ").offset().top;
                  $(".to-scrollZ").removeClass("to-scrollZ"),
                    $(window).width() < 1100 &&
                      $("html, body").scrollTop(e - 60);
                }
              }
            );
        });
    }
    return !1;
  });
}
function Option() {
  $("a.link-pdf, .library-download a").click(function(e) {
    e.preventDefault();
    var t = $(this).attr("href");
    return window.open(t, "_blank"), !1;
  }),
    $(".brochure-box").click(function(e) {
      e.preventDefault();
      var t = $(this)
        .find("a")
        .attr("href");
      return window.open(t, "_blank"), !1;
    }),
    $(".map-link").click(function(e) {
      return (
        e.preventDefault(),
        $("html, body").addClass("no-scroll"),
        $(".logo.mobile").addClass("fixed"),
        $(".googlemap").addClass("show"),
        $("#map-canvas").children().length || initialize(),
        !1
      );
    }),
    $(".close-map").click(function(e) {
      return (
        e.preventDefault(),
        $("html, body").removeClass("no-scroll"),
        $(".logo.mobile").removeClass("fixed"),
        $(".googlemap").removeClass("show"),
        !1
      );
    }),
    $(".view-album, .thumb-album").click(function(e) {
      e.preventDefault();
      var t = $(this).attr("data-href");
      return (
        $(".loadicon").length ||
          $("body").append(
            '<div class="loadicon" style="display:block"></div>'
          ),
        $("html, body").addClass("no-scroll"),
        $(".all-album").fadeIn(100, "linear"),
        $(".overlay-album").css({ display: "block" }),
        $(".overlay-album").animate(
          { height: "100%" },
          800,
          "easeOutExpo",
          function() {
            AlbumLoad(t);
          }
        ),
        !1
      );
    }),
    $("a.player, a.play-video, .home-video").click(function(e) {
      e.preventDefault(),
        $(this)
          .parent()
          .addClass("to-scrollV"),
        $(".popup-video img").length &&
          ($(".popup-pics, .popup-video")
            .removeClass("fadeinup")
            .addClass("fadeout"),
          $(".close-popup")
            .removeClass("fadeinup")
            .addClass("fadeout")),
        $(".library-load").length && $(".library-center").trigger("BTQ.stop");
      var t = $(this).attr("data-href");
      return (
        $(".loadicon").length ||
          $("body").append(
            '<div class="loadicon" style="display:block"></div>'
          ),
        $("html, body").addClass("no-scroll"),
        $(".allvideo").css({ display: "block" }),
        $(".overlay-video").fadeIn(500, "linear", function() {
          $(".allvideo").addClass("show"), VideoLoad(t);
        }),
        !1
      );
    }),
    $(".zoom,  .popup-pics-mobile.no-link-popup img").click(function() {
      $("html, body").addClass("no-scroll"),
        $(this)
          .parent()
          .addClass("to-scroll"),
        $(".loadicon").length ||
          $("body").append(
            '<div class="loadicon" style="display:block"></div>'
          ),
        $(".all-pics").css({ display: "block" }),
        $(".all-pics").append(
          '<div class="full"  style="display:block"></div>'
        ),
        $(".overlay-dark").fadeIn(300, "linear");
      var e =
        $(this)
          .parent()
          .find("img")
          .attr("src") ||
        $(this)
          .parent()
          .find("img")
          .attr("data-src");
      if ($(this).attr("data-src")) var t = $(this).attr("data-src");
      else var t = e;
      return (
        $(".all-pics")
          .find(".full")
          .append('<img src ="' + t + '" alt="pic" />'),
        $(".all-pics")
          .find(".full")
          .append("<span></span>"),
        $("body").append('<div class="close-pics"></div>'),
        $(".all-pics").append('<div class="close-pics-small"></div>'),
        $(".all-pics img").load(function() {
          $(".all-pics").addClass("show"),
            0 != TouchLenght && isTouchDevice
              ? ($(".full").addClass("pinch-zoom"),
                $(".pinch-zoom").each(function() {
                  new Pic.PinchZoom($(this), {});
                }))
              : ($(".full").addClass("dragscroll"),
                $(".dragscroll").draptouch()),
            $(".full img").length > 1 &&
              $(".full img")
                .last()
                .remove(),
            $(".loadicon").fadeOut(400, "linear", function() {
              (0 != TouchLenght && isTouchDevice) || detectMargin(),
                $(".full img").addClass("fadein"),
                $(".loadicon").remove();
            });
        }),
        $(window).width() > 1100 &&
          $(".full span").click(function() {
            $(".close-pics").trigger("click");
          }),
        $(".close-pics, .close-pics-small").click(function() {
          $(".loadicon").remove(),
            $(".full, .close-pics, .close-pics-small").fadeOut(300, "linear"),
            $(".overlay-dark").fadeOut(300, "linear", function() {
              if (
                ($(
                  ".all-pics .full, .all-pics .text-length, .all-pics .pinch-zoom-container"
                ).remove(),
                $(".close-pics, .close-pics-small").remove(),
                $(".all-pics")
                  .css({ display: "none" })
                  .removeClass("show"),
                $("html, body").removeClass("no-scroll"),
                $(".to-scroll").length)
              ) {
                var e = $(".to-scroll").offset().top;
                $(".to-scroll").removeClass("to-scroll"),
                  $(window).width() < 1100 && $("html, body").scrollTop(e - 60);
              }
            });
        }),
        !1
      );
    });
}
function turnWheelTouch() {
  (doWheel = !0), (doTouch = !0);
}
function detectBut() {
  if ($("#news-page").length) {
    if (
      $(window).width() <= 1100 &&
      $(".active .link-page").hasClass("current")
    ) {
      var e = $(".active .link-page.current")
          .parent()
          .parent(),
        t = $(".active .scrollB").offset().left,
        i = $(".active .link-page.current").offset().left,
        o =
          $(".news-list").width() / 2 -
          $(".active .link-page.current").width() / 2;
      $(e)
        .stop()
        .animate({ scrollLeft: i - o - t }, "slow");
    }
    if ($(window).width() <= 1100 && $(".sub-news ul").length) {
      var t = $(".sub-news ul").offset().left,
        i = $(".sub-news li.current").offset().left,
        a = ($(window).width() / 100) * 10,
        o = ($(window).width() - a) / 2 - $(".sub-news li.current").width() / 2;
      $(window).width() / 2 - $(".sub-news li.current").width() / 2;
      $(".sub-news")
        .stop()
        .animate({ scrollLeft: i - o - t }, "slow");
    }
  }
  if ($("#resource-page").length && $(window).width() <= 1100) {
    var t = $(".scrollB").offset().left,
      i = $(".link-page.current").offset().left,
      o =
        $(".news-list.career").width() / 2 -
        $(".link-page.current").width() / 2;
    $(".news-list.career")
      .stop()
      .animate({ scrollLeft: i - o - t }, "slow");
  }
  if ($("#sustainable-page").length && $(window).width() <= 1100) {
    var t = $(".number-slider").offset().left,
      i = $(".number-item.current").offset().left,
      o = $(".number-box").width() / 2 - $(".number-item.current").width() / 2;
    $(".number-box")
      .stop()
      .animate({ scrollLeft: i - o - t }, "slow");
  }
  $(".colum-project:first-child").hasClass("active")
    ? $(".prevslide").addClass("disable")
    : $(".prevslide").removeClass("disable"),
    $(".colum-project:last-child").hasClass("active")
      ? $(".nextslide").addClass("disable")
      : $(".nextslide").removeClass("disable");
}
function detectHeight() {
  if ($(window).width() <= 1100) {
    var e = $(document).innerHeight();
    e > $(window).height() + 100
      ? $(".scroll-down").css({ display: "block", opacity: 1 })
      : $(".scroll-down").css({ display: "none", opacity: 0 });
  }
}
function detectZoom() {
  var e = $(".full img").width(),
    t = ($(".full img").height(), $(window).height(), $(window).width());
  e > t
    ? ($(".show-zoom").addClass("show"), $(".full img").addClass("fullsize"))
    : $(".full img").removeClass("fullsize");
}
function detectMargin() {
  var e = $(".full img").width(),
    t = $(".full  img").height(),
    i = $(window).height(),
    o = $(window).width();
  o > e
    ? $(".full img").css({ "margin-left": o / 2 - e / 2 })
    : $(".full img").css({ "margin-left": 0 }),
    i > t
      ? $(".full img").css({ "margin-top": i / 2 - t / 2 })
      : $(".full img").css({ "margin-top": 0 });
}
!(function(e) {
  var t = { on: e.fn.on, bind: e.fn.bind };
  e.each(t, function(i) {
    e.fn[i] = function() {
      var e,
        o = [].slice.call(arguments),
        a = o.pop(),
        n = o.pop();
      return (
        o.push(function() {
          var t = this,
            i = arguments;
          clearTimeout(e),
            (e = setTimeout(function() {
              n.apply(t, [].slice.call(i));
            }, a));
        }),
        t[i].apply(this, isNaN(a) ? arguments : o)
      );
    };
  });
})(jQuery),
  (function(e) {
    e.fn.clipPath = function() {
      return (
        this.filter("[data-clipPath]").each(function(t) {
          var i = e(this).attr("data-clipPath"),
            o = e(this).attr("src"),
            a = e(this).css("width"),
            n = e(this).css("height"),
            l = parseFloat(a, 10),
            r = parseFloat(n, 10),
            s = e(this).attr("alt"),
            c = e(this).attr("title"),
            d = t,
            u = e(
              '<svg version="1.1" xmlns="http://www.w3.org/2000/svg" class="svgMask" width="' +
                a +
                '" height="' +
                n +
                '" viewBox="0 0 ' +
                l +
                " " +
                r +
                '"><defs><clipPath id="maskID' +
                d +
                '"><path d="' +
                i +
                '"/></clipPath>  </defs><title>' +
                c +
                "</title><desc>" +
                s +
                '</desc><image clip-path="url(#maskID' +
                d +
                ')" width="' +
                a +
                '" height="' +
                n +
                '" xlink:href="' +
                o +
                '" /></svg>'
            );
          e(this).replaceWith(u), delete i, u;
        }),
        this
      );
    };
  })(jQuery);
var timex,
  Click,
  News = 0,
  shownews,
  show,
  show1,
  Menu = 0,
  SubMenu = 0,
  Details = 0,
  doWheel = !0,
  doTouch = !0,
  Has = !0,
  Arrhash,
  windscroll = $(document).scrollTop(),
  monthHash = null,
  date = new Date(),
  curMonth = date.getMonth(),
  curYear = date.getFullYear(),
  timer,
  isLoad = 1;
document.addEventListener("keydown", function(e) {
  var t = e.keyCode || e.which;
  37 === t &&
    ($(".sub-menu li.current")
      .prev()
      .find("a")
      .trigger("click"),
    $(".sub-news li.current")
      .prev()
      .find("a")
      .trigger("click")),
    39 === t &&
      ($(".sub-menu li.current")
        .next()
        .find("a")
        .trigger("click"),
      $(".sub-news li.current")
        .next()
        .find("a")
        .trigger("click"));
}),
  $(document).ready(function() {
    $(document).bind("scroll", function() {
      var e = $(document).scrollTop();
      if ($(window).width() <= 1100) {
        if (
          (e > 50
            ? $(".scroll-down").fadeOut(500, "linear")
            : $(".scroll-down").fadeIn(500, "linear"),
          $("#home-page").length)
        ) {
          var t = $(".slide-bg")[0].swiper;
          e > 100 ? t.stopAutoplay() : (t.startAutoplay(), t.slideNext());
        }
        if (
          (e > $(window).height() / 2
            ? $(".go-top").css({ display: "block", opacity: 1 })
            : $(".go-top").css({ display: "none", opacity: 0 }),
          $(".popup-pics-mobile img").length > 0)
        ) {
          var i = $(".container").css("margin-top"),
            o = parseFloat(i, 10);
          $(".popup-pics-mobile").offset().top,
            $(".mobile-intro").offset().top -
              ($(".mobile-intro").innerHeight() + o);
        }
        windscroll = e;
      }
      var a = document.querySelector(
        ".overlay-menu, .overlay-dark, .overlay-video, .overlay-album"
      );
      a.addEventListener("touchmove", function(e) {
        e.preventDefault();
      });
    }),
      $(".go-top").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
      }),
      $(".container, .overlay-menu").click(function() {
        $(".top").scrollTop(0),
          $(".nav-click").removeClass("active"),
          $(".overlay-menu, .navigation").removeClass("show"),
          $("html, body").removeClass("no-scroll-nav"),
          1 == show &&
            ($(".form-row-search").css({ width: 0 }),
            $(".search-form").css({ width: 0, opacity: 0 }),
            $("a.search").removeClass("active"),
            (show = 0));
      }),
      $(".overlay-menu, .nav").click(function() {
        $(window).width() <= 1100 &&
          ($(".top").scrollTop(0),
          $(".nav-click").removeClass("active"),
          $(".overlay-menu, .navigation").removeClass("show"),
          $("html, body").removeClass("no-scroll-nav"));
      });
  }),
  (window.onorientationchange = ResizeWindows),
  $(window).on("orientationchange", function() {
    $(window).width() <= 1100 &&
      (ScrollHoz(),
      $(
        "#projects-page, #project-details-page, #sustainable-page, #resource-page"
      ).length && detectBut(),
      $(".zoom-pic").length &&
        $(window).width() > 740 &&
        ($("html, body").removeClass("no-scroll"),
        $(".close-pics-small").trigger("click")));
  }),
  $(window).resize(function() {
    ScrollNiceHide(), ResizeWindows();
  }),
  $(window).on(
    "resize",
    function() {
      if ((ResizeWindows(), $(window).width() > 1100))
        Calculation(),
          $(".dragscroll").length &&
            (detectMargin(), $(".dragscroll").draptouch()),
          $("#home-page").length &&
            ($(".item-active").length || SlidePicture(),
            ($(".wrap-sustainable .show-box").length ||
              $(".wrap-project .show-pro").length) &&
              ($(".wrap-sustainable")
                .data("BTQSlider")
                .destroy(),
              $(".wrap-project")
                .data("BTQSlider")
                .destroy())),
          $("#about-page").length && ($(".ani-text").length || SlidePicture()),
          $("#sustainable-page").length &&
            ($(".ani-sus").length || SlidePicture(),
            $(".sub-nav li.current").length
              ? setTimeout(function() {
                  $(".sub-nav li.current").trigger("click");
                }, 150)
              : setTimeout(function() {
                  $(".sub-nav li:first-child").trigger("click");
                }, 150)),
          $("#project-page").length && ($(".ani-pro").length || SlidePicture()),
          $("#resource-page").length &&
            ($(".ani-resource").length || SlidePicture()),
          $("#member-page").length &&
            ($(".show-member").length || SlidePicture()),
          $("#business-page, #search-page").length &&
            ($(".show-member").length || SlidePicture()),
          $("#project-details-page").length &&
            ($(".sub-menu li").hasClass("current")
              ? $(".sub-menu li.current a").trigger("click")
              : $(".sub-menu li:first-child a").trigger("click"),
            $(".slide-pro").length &&
              ($(".slide-pro").each(function(e, t) {
                $(t)
                  .data("BTQSlider")
                  .destroy();
              }),
              setTimeout(function() {
                $(".grid").removeClass("slide-pro");
              }, 100)),
            $(".arrange").length ||
              setTimeout(function() {
                LoadProjects();
              }, 100)),
          $(".news-list, .number-box").hasClass("dragscroll") &&
            ($(".news-list, .number-box").removeClass(
              "dragscroll draptouch-active draptouch-moving-left draptouch-moving-down"
            ),
            $(".news-list, .number-box").css({ overflow: "visible" })),
          $(
            ".scrollA, .scrollB, .scrollC, .scrollD, .scrollE, .scrollF, .scrollG"
          ).length &&
            ($(".scrollB").css({ width: "100%" }),
            setTimeout(function() {
              ScrollNiceA(),
                ScrollNiceB(),
                ScrollNiceC(),
                ScrollNiceD(),
                ScrollNiceE(),
                ScrollNiceF(),
                ScrollNiceG();
            }, 150));
      else {
        if (
          (detectHeight(),
          (0 != TouchLenght && isTouchDevice) ||
            (Calculation(),
            ScrollHoz(),
            $("#news-page, #resource-page, #sustainable-page").length &&
              detectBut(),
            $(window).width() > 740 &&
              $(".all-pics").hasClass("show") &&
              $(".close-pics-small, .close-pics").trigger("click")),
          $("#home-page").length)
        ) {
          if ($(".item-active").length) {
            var e = $(".home-slide")[0].swiper;
            e.destroy(!0, !0);
          }
          ($(".wrap-sustainable .show-box").length &&
            $(".wrap-project .show-pro").length) ||
            SlidePicture();
        }
        if ($("#about-page").length && $(".ani-text").length) {
          var t = $(".slider-about")[0].swiper,
            i = $(".history-slide")[0].swiper;
          t.destroy(!0, !0),
            i.destroy(!0, !0),
            $(".content-center").removeClass("ani-text");
        }
        if ($("#sustainable-page").length) {
          if ($(".ani-sus").length) {
            var o = $(".slider-sustainable")[0].swiper;
            o.destroy(!0, !0), $(".content-center").removeClass("ani-sus");
          }
          $(".number-slider.slide-slidebox").length &&
            $(".number-slider").each(function(e, t) {
              $(t)
                .data("BTQSlider")
                .destroy();
            });
        }
        if ($("#resource-page").length && $(".ani-resource").length) {
          var o = $(".slider-resource")[0].swiper;
          o.destroy(!0, !0), $(".content-center").removeClass("ani-resource");
        }
        if ($("#project-page").length && $(".ani-pro").length) {
          var a = $(".slider-project")[0].swiper;
          a.destroy(!0, !0), $(".content-center").removeClass("ani-pro");
        }
        if (
          ($("#project-details-page").length &&
            ($(".arrange").length &&
              ($(".grid").isotope("destroy"),
              $(".pic-project").removeClass("arrange"),
              $(".close").trigger("click")),
            $(".slide-pro").length ||
              ($(".grid").addClass("slide-pro"), SlidePicture())),
          $("#member-page").length && $(".show-member").length)
        ) {
          var n = $(".slider-member")[0].swiper;
          n.destroy(!0, !0), $(".content-center").removeClass("show-member");
        }
        $("#business-page, #search-page").length &&
          $(".show-member").length &&
          ($(".slider-member")
            .data("BTQSlider")
            .destroy(),
          $(".slider-member").addClass("fadein")),
          $(".dragscroll").length &&
            (detectMargin(), $(".dragscroll").draptouch());
      }
      if ($("#news-page").length) {
        var l = $(".colum-box.active").attr("data-hash");
        $(".sub-news li a[data-name='" + l + "']").trigger("click");
      }
    },
    250
  ),
  $(window).bind("popstate", function(e) {
    $(window).width() > 1100 && (e.preventDefault(), LinkPage());
    var t = $(".httpserver").text();
    if ($(window).width() > 1100)
      if (null !== e.originalEvent.state) {

        var l = i.replace(t, ""),
          r = l.split("/");
        $("#about-page, #member-page, #business-page, #sustainable-page")
          .length &&
          ($(".close-pics").length && $(".close-pics").trigger("click"),
          $(".nav li a").each(function(e, t) {
            $(t).attr("href") == i && window.history.back();
          }),
          $(".sub-nav li a").each(function(e, t) {
            $(t).attr("href") == i && $(t).trigger("click");
          })),
          $("#news-page").length &&
            ($(".close-album").length && $(".close-album").trigger("click"),
            $(".close-video").length && $(".close-video").trigger("click"),
            "" != r[2] && void 0 != r[2]
              ? ($(".sub-news li.current a").attr("href") !=
                  t + r[0] + "/" + r[1] + ".html" &&
                  $(
                    ".sub-news li a[href='" + t + r[0] + "/" + r[1] + ".html']"
                  ).trigger("click"),
                $(".link-page a").each(function(e, t) {
                  $(t).attr("href") == i && $(t).trigger("click");
                }))
              : "" != r[1] && void 0 != r[1]
                ? "videos" !=
                    $(".sub-news li a[href='" + i + "']").attr("data-name") &&
                  "pictures" !=
                    $(".sub-news li a[href='" + i + "']").attr("data-name")
                  ? window.history.back()
                  : $(".sub-news li a[href='" + i + "']").trigger("click")
                : window.history.back()),
          $("#resource-page").length &&
            ("" != r[2] && void 0 != r[2]
              ? ($(".sub-nav li.current a").attr("href") !=
                  t + r[0] + "/" + r[1] + ".html" &&
                  $(
                    ".sub-nav li a[href='" + t + r[0] + "/" + r[1] + ".html']"
                  ).trigger("click"),
                $(".link-career").each(function(e, t) {
                  $(t).attr("href") == i && $(t).trigger("click");
                }))
              : "" != r[1] &&
                void 0 != r[1] &&
                $(".sub-nav li.current a").attr("href") != t + r[0] + "/" + r[1]
                ? $(
                    ".sub-nav li a[href='" + t + r[0] + "/" + r[1] + "']"
                  ).trigger("click")
                : window.history.back()),
          $("#project-details-page").length &&
            ($(".close-pics").length && $(".close-pics").trigger("click"),
            $(".close-album").length && $(".close-album").trigger("click"),
            $(".close-video").length && $(".close-video").trigger("click"),
            $(".nav li a").each(function(e, t) {
              $(t).attr("href") == i && window.history.back();
            }),
            $(".sub-menu li a").each(function(e, t) {
              $(t).attr("href") == i && $(t).trigger("click");
            }),
            r.length <= 2 && window.history.back());
      } else {
        var n = document.URL,
          l = n.replace(t, ""),
          r = l.split("/");
        $("#about-page, #member-page, #business-page, #sustainable-page")
          .length &&
          ($(".close-pics").length && $(".close-pics").trigger("click"),
          $(".nav li a").each(function(e, t) {
            $(t).attr("href") == n && window.history.back();
          }),
          $(".sub-nav li a").each(function(e, t) {
            $(t).attr("href") == n && $(t).trigger("click");
          })),
          $("#news-page").length &&
            ($(".close-album").length && $(".close-album").trigger("click"),
            $(".close-video").length && $(".close-video").trigger("click"),
            "" != r[2] && void 0 != r[2]
              ? ($(".sub-news li.current a").attr("href") !=
                  t + r[0] + "/" + r[1] + ".html" &&
                  $(
                    ".sub-news li a[href='" + t + r[0] + "/" + r[1] + ".html']"
                  ).trigger("click"),
                $(".link-page a").each(function(e, t) {
                  $(t).attr("href") == n && $(t).trigger("click");
                }))
              : "" != r[1] && void 0 != r[1]
                ? "videos" !=
                    $(".sub-news li a[href='" + n + "']").attr("data-name") &&
                  "pictures" !=
                    $(".sub-news li a[href='" + n + "']").attr("data-name")
                  ? window.history.back()
                  : $(".sub-news li a[href='" + n + "']").trigger("click")
                : window.history.back()),
          $("#resource-page").length &&
            ("" != r[2] && void 0 != r[2]
              ? ($(".sub-nav li.current a").attr("href") !=
                  t + r[0] + "/" + r[1] + ".html" &&
                  $(
                    ".sub-nav li a[href='" + t + r[0] + "/" + r[1] + ".html']"
                  ).trigger("click"),
                $(".link-career").each(function(e, t) {
                  $(t).attr("href") == n && $(t).trigger("click");
                }))
              : "" != r[1] &&
                void 0 != r[1] &&
                $(".sub-nav li.current a").attr("href") != t + r[0] + "/" + r[1]
                ? $(
                    ".sub-nav li a[href='" + t + r[0] + "/" + r[1] + "']"
                  ).trigger("click")
                : window.history.back()),
          $("#project-details-page").length &&
            ($(".close-pics").length && $(".close-pics").trigger("click"),
            $(".close-album").length && $(".close-album").trigger("click"),
            $(".close-video").length && $(".close-video").trigger("click"),
            $(".nav li a").each(function(e, t) {
              $(t).attr("href") == n && window.history.back();
            }),
            $(".sub-menu li a").each(function(e, t) {
              $(t).attr("href") == n && $(t).trigger("click");
            }),
            r.length <= 2 && window.history.back());
      }
    else {
      if (null !== e.originalEvent.state) var i = e.originalEvent.state.path;
      else var i = document.URL;
      var l = i.replace(t, ""),
        r = l.split("/");
      $("#news-page").length &&
        ($(".close-album").length && $(".close-album").trigger("click"),
        $(".close-video").length && $(".close-video").trigger("click"),
        "" != r[2] && void 0 != r[2]
          ? ($(".sub-news li.current a").attr("href") !=
              t + r[0] + "/" + r[1] + ".html" && location.reload(),
            $(".link-page a").each(function(e, t) {
              $(t).attr("href") == i && location.reload();
            }))
          : "" != r[1] && void 0 != r[1]
            ? "videos" !=
                $(".sub-news li a[href='" + i + "']").attr("data-name") &&
              "pictures" !=
                $(".sub-news li a[href='" + i + "']").attr("data-name")
              ? window.history.back()
              : location.reload()
            : window.history.back()),
        $("#product-details-page").length && location.reload(),
        $("#about-page, #member-page, #business-page, #sustainable-page")
          .length && location.reload(),
        $("#resource-page").length &&
          (r.length > 1 ? location.reload() : window.history.back());
    }
  }),
  iOS &&
    $(window).bind("pageshow", function(e) {
      e.originalEvent.persisted && window.location.reload();
    });
