var Q = $("#preloader"),
  X = $("#logo-svg-loading");
Q.length > 0 &&
  $(window).on("load", function() {
    $("body").addClass("logo-loading");

    setTimeout(function() {
      X.fadeOut(300);
      $("body")
        .removeClass("logo-loading")
        .addClass("loaded");
      Q.delay(700).fadeOut(300);
    }, 2000);
  });

var htmlElement = document.documentElement;
var bodyElement = document.body;
var pageOverlay = document.getElementById("overlay");

var FOODY = {
  // _API: "https://publicapi.vienthonga.vn",
  // _URL: "https://vienthonga.vn"
};

var Layout = (function() {
  var setUserAgent = function() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    // Windows Phone must come first because its UA also contains "Android"
    if (/windows phone/i.test(userAgent)) {
      bodyElement.classList.add("window-phone");
      return;
    }

    if (/android/i.test(userAgent)) {
      bodyElement.classList.add("android");
      return;
    }

    // iOS detection from: http://stackoverflow.com/a/9039885/177710
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
      bodyElement.classList.add("ios");
      return;
    }
  };

  var scrollToTop = function() {
    var scrollTop = $("#scrollToTop");

    $(window).on("scroll", function() {
      if ($(window).scrollTop() > 200) {
        scrollTop.addClass("active");
      } else {
        scrollTop.removeClass("active");
      }
    });

    scrollTop.click(function() {
      $("body, html").animate(
        {
          scrollTop: 0
        },
        500
      );
    });
  };

  return {
    init: function() {
      setUserAgent();
      scrollToTop();
    }
  };
})();

$(document).ready(function() {
  Layout.init();
});

var slide = [];

slide.push({ src: "images/banner/banner1.jpg" });

slide.push({ src: "images/banner/banner2.jpg" });

$("#slider").vegas({
  slides: slide,
  timer: false,
  animationDuration: 10000,
  animation: ["fade"]
});

$("#wave").wavify({
  height: 70,
  bones: 4,
  amplitude: 20,
  color: "rgba(167,14,19,0.8)",
  speed: 0.5
});

$("#wave2").wavify({
  height: 60,
  bones: 3,
  amplitude: 30,
  color: "rgba(167,14,19,0.6)",
  speed: 0.3
});

$(document).ready(function() {
  var swiper = new Swiper(".project-f-slider-container", {
    slidesPerView: 3,
    spaceBetween: 10,
    navigation: {
      nextEl: ".project-f-next",
      prevEl: ".project-f-prev"
    },
    breakpoints: {
      1024: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      640: {
        spaceBetween: 0,
        slidesPerView: 1,
        spaceBetween: 10
      },
      320: {
        spaceBetween: 0,
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  });

  var swiper = new Swiper(".news-f-slider", {
    slidesPerView: 3,
    spaceBetween: 2,
    navigation: {
      nextEl: ".news-f-next",
      prevEl: ".news-f-prev"
    },
    pagination: {
      el: ".swiper-pagination"
    },
    breakpoints: {
      1024: {
        slidesPerView: 2,
        spaceBetween: 2
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 2
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 2
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 2
      }
    }
  });

  var projectSlider = new Swiper(".project-slider-swiper", {
    mousewheel: true,
    navigation: {
      prevEl: ".project-prev",
      nextEl: ".project-next"
    }
  });

  var izNew = new Swiper(".iz-date-swiper", {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    effect: "fade",
    fadeEffect: {
      crossFade: true
    },
    autoplay: {
      delay: 5000,
      disableOnInteraction: false
    }
  });

  var swiper = new Swiper(".news-slider", {
    slidesPerView: 3,
    spaceBetween: 20,
    navigation: {
      nextEl: ".news-next",
      prevEl: ".news-prev"
    },
    breakpoints: {
      1024: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      768: {
        slidesPerView: 2,
        spaceBetween: 10
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 10
      },
      320: {
        slidesPerView: 1,
        spaceBetween: 10
      }
    }
  });
});

$(document).ready(function() {

  $('.selectpicker').selectpicker();
  $.fn.selectpicker.Constructor.BootstrapVersion = '4';


  $("#range_03").ionRangeSlider({
    type: "double",
    min: 0,
    max: 2000000000,
    from: 250000000,
    step: 250000000,
    prettify_separator: ",",
    force_edges: true,
    prefix: "₫ "
});


  var izShow = $(".iz-container .iz");
  izShow.hover(
    function() {
      $(this).addClass("is-show");
      izShow.addClass("is-hover");
    },
    function() {
      $(this).removeClass("is-show");
      izShow.removeClass("is-hover");
    }
  );

  var izShowCount = $(".iz-container .iz").length; //2
  var izActiveIndex = 0;
  setInterval(function() {
    if ($(".iz-container .iz").index($(".iz.active")) == izShowCount - 1) {
      izActiveIndex = 0;
    } else {
      izActiveIndex++;
    }
    $(".iz-container .iz.active").removeClass("active");
    $(".iz-container .iz")
      .eq(izActiveIndex)
      .addClass("active");
  }, 10000);

  var K = $(".animated");
  if (K.length > 0) {
    K.each(function() {
      var e = $(this),
        i = e.data("animate"),
        l = e.data("duration"),
        t = e.data("delay");
      e.waypoint(
        function() {
          e.addClass("animated " + i).css("visibility", "visible"),
            l && e.css("animation-duration", l + "s"),
            t && e.css("animation-delay", t + "s");
        },
        {
          offset: "93%"
        }
      );
    });
  }
});
